package com.zuo;

import java.util.ArrayList;

public class ZuoArray {
    public static void main(String[] args) {

        ArrayList<Integer> intList = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            intList.add(i);
        }
        System.out.println(intList);
        for (Integer i :
                intList) {
        }
        System.out.println(intList);


        ArrayList<String> strlist = new ArrayList<String>();
        strlist.add("老马");
        strlist.add("编程");
        for (String str :
                strlist) {
            System.out.println(str);
        }
    }
}
