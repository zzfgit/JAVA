package com.zzf;

import java.util.Arrays;

public class DynamicArray<E> {
    /*默认数组容量*/
    public static final int DEFAULT_CAPACITY = 10;
    /*数组长度*/
    private int size;
    /*内部私有数组*/
    private Object[] elementData;
    /*构造方法*/
    public DynamicArray(){
        this.elementData = new Object[DEFAULT_CAPACITY];
    }
    private void ensureCapacity(int minCapacity){
        int oldCapacity = elementData.length;
        if (oldCapacity >= minCapacity){
            return;
        }
        int newCapacity = oldCapacity * 2;
        if (newCapacity < minCapacity)
            newCapacity = minCapacity;
        elementData = Arrays.copyOf(elementData,newCapacity);

    }
    public void add (E e){
        ensureCapacity(size + 1);
        elementData[size++] = e;
    }
    public E get (int index){
        return (E)elementData[index];
    }
    public int size(){
        return size;
    }
    public E set(int index,E element){
        E oldvalue = get(index);
        elementData[index] = element;
        return oldvalue;
    }

}

