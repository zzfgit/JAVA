package com.zzf;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        MyComparable p1 = new Point(2,3);
        MyComparable p2 = new Point(1, 2);

        System.out.println("p2 = " + p2);


        DynamicArray<Double> arr = new DynamicArray<Double>();
        Random rnd = new Random();
        int size = 1 + rnd.nextInt(100);
        for (int i = 0; i < size; i++) {
            arr.add(rnd.nextDouble());
        }
        Double d = arr.get(rnd.nextInt(size));

        for (int i = 0; i < size; i++) {
            System.out.println( i + arr.get(i));
        }
    }

}

