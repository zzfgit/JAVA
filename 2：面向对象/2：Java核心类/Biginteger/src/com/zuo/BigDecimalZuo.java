package com.zuo;

import java.math.BigDecimal;
import java.math.RoundingMode;

/*
* 和BigInteger类似，BigDecimal可以表示一个任意大小且精度完全准确的浮点数。


 * */
public class BigDecimalZuo {
    public static void main(String[] args) {

        BigDecimal bd = new BigDecimal("123.4567");
        System.out.println(bd.multiply(bd));


//        BigDecimal用scale()表示小数位数，例如：
        BigDecimal d1 = new BigDecimal("123.45");
        BigDecimal d2 = new BigDecimal("123.4500");
        BigDecimal d3 = new BigDecimal("12343500");

        System.out.println(d1.scale());//2
        System.out.println(d2.scale());//4
        System.out.println(d3.scale());//0

//        通过BigDecimal的stripTrailingZeros()方法，可以将一个BigDecimal格式化为一个相等的，但去掉了末尾0的BigDecimal：
        BigDecimal d4 = new BigDecimal("123.4500");
        BigDecimal d5 = d4.stripTrailingZeros();
        System.out.println(d4.scale()); // 4
        System.out.println(d5.scale()); // 2,因为去掉了00

        BigDecimal d6 = new BigDecimal("1234500");
        BigDecimal d7 = d6.stripTrailingZeros();
        System.out.println(d6.scale()); // 0
        System.out.println(d7.scale()); // -2

//        可以对一个BigDecimal设置它的scale，如果精度比原始值低，那么按照指定的方法进行四舍五入或者直接截断：
        BigDecimal d8 = new BigDecimal("123.456789");
        BigDecimal d9 = d8.setScale(4, RoundingMode.HALF_UP);//四舍五入，123.4568
        BigDecimal d10 = d8.setScale(4, RoundingMode.DOWN);//直接截断，123.4567

        System.out.println(d9);
        System.out.println(d10);

//        对BigDecimal做加、减、乘时，精度不会丢失，但是做除法时，存在无法除尽的情况，这时，就必须指定精度以及如何进行截断：
        BigDecimal d11 = new BigDecimal("123.456");
        BigDecimal d12 = new BigDecimal("23.456789");
        BigDecimal d13 = d1.divide(d2, 10, RoundingMode.HALF_UP); // 保留10位小数并四舍五入

//        在比较两个BigDecimal的值是否相等时，要特别注意，使用equals()方法不但要求两个BigDecimal的值相等，还要求它们的scale()相等：
        BigDecimal d14 = new BigDecimal("123.456");
        BigDecimal d15 = new BigDecimal("123.45600");
        System.out.println(d14.equals(d15)); // false,因为scale不同
        System.out.println(d14.equals(d15.stripTrailingZeros())); // true,因为d2去除尾部0后scale变为2
        System.out.println(d14.compareTo(d15)); // 0

//        使用compareTo()方法来比较大小，它根据两个值的大小分别返回负数、正数和0，分别表示小于、大于和等于。

    }
}
