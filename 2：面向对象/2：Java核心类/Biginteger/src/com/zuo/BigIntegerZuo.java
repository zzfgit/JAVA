package com.zuo;

import java.math.BigInteger;

/*
* BigInteger
*在Java中，由CPU原生提供的整型最大范围是64位long型整数。使用long型整数可以直接通过CPU指令进行计算，速度非常快。
如果我们使用的整数范围超过了long型怎么办？这个时候，就只能用软件来模拟一个大整数。java.math.BigInteger就是用来表示任意大小的整数。BigInteger内部用一个int[]数组来模拟一个非常大的整数：
*
* */
public class BigIntegerZuo {
    public static void main(String[] args) {

        BigInteger bi = new BigInteger("1234567890");
        System.out.println(bi.pow(5));
//        对BigInteger做运算的时候，只能使用实例方法，例如，加法运算：
        BigInteger bi1 = new BigInteger("1234567890");
        BigInteger bi2 = new BigInteger("12345678901234567890");
        BigInteger biSum = bi1.add(bi2);
        System.out.println("biSum = " + biSum);
//        和long型整数运算比，BigInteger不会有范围限制，但缺点是速度比较慢。

//        也可以把BigInteger转换成long型：
        BigInteger bi3 = new BigInteger("1234567890");
        System.out.println("bi3.long = " + bi3.longValue());

        /*
        * BigInteger和Integer、Long一样，也是不可变类，并且也继承自Number类。因为Number定义了转换为基本类型的几个方法：
        *
        *转换为byte：byteValue()
        *转换为short：shortValue()
        *转换为int：intValue()
        *转换为long：longValue()
        *转换为float：floatValue()
        *转换为double：doubleValue()
        *
        * */

        BigInteger n = new BigInteger("999999").pow(99);
        float f = n.floatValue();
        System.out.println(f); //Infinity 无限
    }
}
