package com.zuo;

/*
* 这就是一个JavaBean类
*
* boolean字段比较特殊,它的读方法一般以is开头
*
* 我们通常把一组对应的读方法和写方法称为属性
* 只有一个getter方法的属性称为只读属性
* 只有一个setter方法的属性称为只写属性
* 只读属性很常见,只写属性不常见
*
* JavaBean的作用
* JavaBean主要用来传递数据，即把一组数据组合成一个JavaBean便于传输。此外，JavaBean可以方便地被IDE工具分析，生成读写属性的代码，主要用在图形界面的可视化设计中。
*
* */
public class Person {
    private String name;
    private int age;
    private boolean chiled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isChiled() {
        return chiled;
    }

    public void setChiled(boolean chiled) {
        this.chiled = chiled;
    }
}
