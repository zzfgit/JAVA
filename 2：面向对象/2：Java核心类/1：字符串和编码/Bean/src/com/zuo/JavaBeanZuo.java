package com.zuo;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

/*
* 在Java中，有很多class的定义都符合这样的规范：

若干private实例字段；
通过public方法来读写实例字段。
*
*
* 如果读写方法符合以下这种命名规范：
// 读方法:
public Type getXyz()
// 写方法:
public void setXyz(Type value)
* 那么这种class被称为JavaBean：
*
*
* 要枚举一个javabean的所有属性,可以直接使用Java核心库提供的Introspector(内省)
*
* */
public class JavaBeanZuo {

    public static void main(String[] args) throws Exception {

//        使用Introspector(内省)来遍历一个javabean中所有的属性,和读写方法
        BeanInfo info = Introspector.getBeanInfo(Person.class);
        for (PropertyDescriptor pd :
                info.getPropertyDescriptors()) {
            System.out.println(pd.getName());
            System.out.println("  " + pd.getReadMethod());
            System.out.println("  " + pd.getWriteMethod());
        }
    }
}
