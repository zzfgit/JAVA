package com.zuo;


public class StringBuilderZuo {
    public static void main(String[] args) {

//        Java编译器对String做了特殊处理，使得我们可以直接用+拼接字符串。
        String s = "";
        for (int i = 0; i < 1000; i++) {
            s = s + "," + i;
        }
        System.out.println(s);
//        虽然可以直接拼接字符串，但是，在循环中，每次循环都会创建新的字符串对象，然后扔掉旧的字符串。这样，绝大部分字符串都是临时对象，不但浪费内存，还会影响GC效率。
//        为了能高效拼接字符串，Java标准库提供了StringBuilder，它是一个可变对象，可以预分配缓冲区，这样，往StringBuilder中新增字符时，不会创建新的临时对象：
        StringBuilder sb = new StringBuilder(1024);
        for (int i = 0; i < 1000; i++) {
            sb.append(',');
            sb.append(i);
        }
        String sbS = sb.toString();
        System.out.println(sbS);
//        StringBuilder还可以进行链式操作：
//        如果我们查看StringBuilder的源码，可以发现，进行链式操作的关键是，定义的append()方法会返回this，这样，就可以不断调用自身的其他方法。
        var sb2 = new StringBuilder(1024);
        sb2.append("Mr ").append("bob").insert(0,"hello, ");
        System.out.println("sb2 = " + sb2.toString());

//        练习1:自己设计一个可以链式操作的类
        Adder add = new Adder();
        add = add.inc().inc().inc().inc();
        System.out.println("add.value = " + add.value());
        add.add(3).add(3).add(3);
        System.out.println("add.value = " + add.value());

//        练习2:请使用StringBuilder构造一个INSERT语句：
        String[] fields = {"name", "position", "salary"};
        String table = "employee";
        String insert = buildInsertSql(table,fields);
        System.out.println(insert);
        String s1 = "INSERT INTO employee (name, position, salary) VALUES (?, ?, ?)";
        System.out.println(s1);
        System.out.println(s1.equals(insert) ? "测试成功" : "测试失败");
    }

    //练习2
    //请使用StringBuilder构造一个INSERT语句：
    static String buildInsertSql(String table, String[] fields) {
        // TODO:
        StringBuilder s = new StringBuilder(100);
        s.append("INSERT INTO ").append(table).append(" (");
        for(String st:fields) {
            s.append(st).append(", ");
        }
        s.delete(s.length()-2,s.length()).append(") VALUES (?, ?, ?)");
        String ss = s.toString();
        return ss;
    }
}


//练习1:设计一个可以链式操作的类
class Adder {
    private int sum = 0;
    public Adder add(int n){
        sum += n;
        return this;
    }

    public Adder inc(){
        sum ++;
        return this;
    }

    public int value(){
        return sum;
    }
}


