package com.zuo;
/*
* 使用分隔符来拼接数组,Java提供了StringJoiner这个关键字
*
* */

import java.util.StringJoiner;

public class StringJoinerZuo {
    public static void main(String[] args) {

        String[] names = {"Bob","Alice","Grace"};
//        使用 "," 作为分隔符拼接数组为一个字符串
        var sj = new StringJoiner(",");
        for (String name :
                names) {
            sj.add(name);
        }
        System.out.println(sj.toString());

//        使用StringJoiner拼接字符串,还可以添加开始和结尾字符
        String[] names1 = {"AAA","BBB","CCC"};
        var sj1 = new StringJoiner(",","Hello","!");
        for (String name :
                names1) {
            sj1.add(name);
        }
        System.out.println(sj1.toString());

//        还可以使用String.join()这个静态方法,更加方便
        String[] names2 = {"AAA","BBB","CCC"};
        String s = String.join(",",names2);
        System.out.println(s);


//        练习:请使用StringJoiner构造一个SELECT语句：
        String[] fields = { "name", "position", "salary" };
        String table = "employee";
        String select = buildSelectSql(table, fields);
        System.out.println(select);
        System.out.println("SELECT name, position, salary FROM employee".equals(select) ? "测试成功" : "测试失败");

    }

    //练习:请使用StringJoiner构造一个SELECT语句：
    static String buildSelectSql(String table,String[] fields) {
        var sj = new StringJoiner(", ","SELECT "," FROM employee");
        for (String field :
                fields) {
            sj.add(field);
        }
        return sj.toString();
    }
}
