/**
 * TaxTest
 */
public class TaxTest {

    public static void main(String[] args) {
        // 给一个有普通收入、工资收入和享受国务院特殊津贴的小伙伴算税:
        Income[] incomes = new Income[]{
            new Income(3000),
            new Salary(7500),
            new StateCouncilSpecialAllowance(15000)
        };

        System.out.println(totalTax(incomes)); //800
    }
    public static double totalTax(Income... incomes) {
        double total = 0;
        for (Income income : incomes) {
            total = total + income.getTax();
        }
        return total;
    }
}

/**
 * 收入类
 * income 收入
 * 收税收入的十分之一
 */
class Income {
    protected double income;

    public Income(double income) {
        this.income = income;
    }
    public double getTax() {
        return income * 0.1;
    }
}

/**
 * 工资类
 * 超过5000收税,收多余5000的部分的百分之二十
 * 不超过5000不收税
 */
class Salary extends Income {
    public Salary (double income) {
        super(income);
    }

    @Override
    public double getTax() {
        if (income <= 5000) {
            return 0;
        }
        return (income - 5000) * 0.2;
    }
}

/**
 * 国务院补贴,不用交税
 */
class StateCouncilSpecialAllowance extends Income {
    public StateCouncilSpecialAllowance(double income) {
        super(income);
    }
    @Override
    public double getTax() {
        return 0;
    }
}