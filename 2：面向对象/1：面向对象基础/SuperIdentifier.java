/**
 * SuperIdentifier
 */
public class SuperIdentifier {

    public static void main(String[] args) {
        Student4 xiaoWang = new Student4("xiaoWang", 18, 90);
        System.out.println(xiaoWang.name);
        System.out.println(xiaoWang.age);
        System.out.println(xiaoWang.score);


        Person4 xiao4 = new Person4("xiao4", 4);
        System.out.println(xiao4.name);
        System.out.println(xiao4.age);
    }
}

class Person4 {
    protected String name;
    protected int age;

    public Person4(String name, int age){
        this.name = name;
        this.age = age;
    }
}

class Student4 extends Person4 {
    protected int score;

    public Student4(String name, int age, int score){
        // 在Java中，任何class的构造方法，第一行语句必须是调用父类的构造方法。如果没有明确地调用父类的构造方法，编译器会帮我们自动加一句super()
        // 但是，如果父类并没有无参数的构造方法，就会编译失败。
        //解决方法是调用父类存在的某个构造方法
        //这样就可以编译通过
        super(name, age);
        this.score = score;
    }
}