package zuo;
/*
* 包名：package
* 在不同包中的类可以是相同的类名，不会冲突
* 在同一个包中，不能有重名的类，会起冲突
* */
/*
* 在一个class中，我们总会引用其他的class
* 如果要引用其他的类，有三这个写法
* 1：直接写出完整类名
* 2：使用import语句，然后使用简单类名
* 3：使用import static的语法，导入一个类的静态字段和静态方法，这种一般很少使用
* */

//导入包li中的Student类
//在使用import的时候，可以使用*，表示把包里所有的class都导入进来，但不包括子包的class，一般不推荐这种做法，因为导入太多class的话，很难看出class属于哪个包
import li.Student;

public class Person {
    public static void main(String[] args) {
        Person xiaozuo = new Person();
        xiaozuo.run();

        //1:引用其他类的第一种用法：写出完整类名
        li.Person xiaoli = new li.Person();
        xiaoli.run();

        Student xiaoming = new Student();
        xiaoming.study();
    }
    public void run(){
        System.out.println("xiaozuo run");
    }
}
