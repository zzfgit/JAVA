/**
 * InterfaceTest
 * 练习题:用接口实现不同的收入计税
 */
public class InterfaceTest {

    public static void main(String[] args) {
        Income[] incomes = new Income[] {
            new Income(3000),
            new SalaryIncome(7500),
            new RoyalIncome(12000)
        };

        double totalTax = 0;
        for (Income income : incomes) {
            double tax = income.getTax();
            System.out.println(tax);
            totalTax = totalTax + tax;
        }
        System.out.println("总共需要缴税: " + totalTax);
    }
} 

/**
 * 定义接口Income
 */
interface IncomeInterface {
     double getTax();
}

/**
 * 普通收入类:税率10%
 */
class Income implements IncomeInterface{
    protected double income;

    public Income(double income){
        this.income = income;
    }
    @Override
    public double getTax() {
        return this.income * 0.1;
    }
}

/**
 * 稿费收入类
 * 税率是20%
 */
class RoyalIncome extends Income implements IncomeInterface{

    public RoyalIncome(double income){
        super(income);
    }

    @Override
    public double getTax() {
        return this.income * 0.2;
    }
}

/**
 * 工资收入类
 * 5000以上部分收税10%,5000以内不收税
 */
class SalaryIncome extends Income implements IncomeInterface{

    public SalaryIncome (double income) {
        super(income);
    }

    @Override
    public double getTax() {
        if (this.income > 5000) {
            return (this.income - 5000) * 0.1;
        }else{
            return 0;
        }
    }
}

