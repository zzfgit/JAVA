/**
 * Inherit
 * 继承
 * 继承使用extends关键字来实现
 * Java中除了object类所有的类都有父类,如果没有写就是继承自object,object类没有父类
 * 子类继承自父类,就拥有了父类的public修饰的属性和方法,但是private修饰的属性和方法,子类是无法访问的
 * 
 */


public class Inherit {

    public static void main(String[] args) {
        
        Student xiaoming = new Student();
        xiaoming.setScore(90);
        System.out.println(xiaoming.name);
        System.out.println(xiaoming.age);
        System.out.println(xiaoming.getScore());

        System.out.println(xiaoming.hello());
    }
}

/**
 * 子类无法访问父类的用private修饰的属性
 * 但是可以访问用protected修饰的属性,子类的子类也可以访问
 * 
 */

class Student extends Person3{
    //不需要再重复定义name和age字段
    //只需要定义新的字段和功能
    private int score;

    public int getScore() {
        return this.score;
    }
    public void setScore(int score) {
        this.score = score;
    }

    //使用super关键字,可以访问父类的属性
    // 其实这里不用super也可以,因为编译器会自动找到父类的name属性
    public String hello() {
        return "Hello, " + super.name;
    }
}