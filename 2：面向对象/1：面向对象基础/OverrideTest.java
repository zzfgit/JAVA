/**
 * OverrideTest
 * 覆写练习题
 * 给一个有工资收入和稿费收入的小伙伴算税
 */
public class OverrideTest {

    public static void main(String[] args) {
        
        Income[] incomes = new Income[] {
            new Income(3000),
            new Salary(7500),
            new RoyaltyIncome(12000)
        };
        double totalTax = 0;

        // TODO
        for (Income income : incomes) {
            totalTax = totalTax + income.getTax();
        }
        System.out.println(totalTax);
        // 最后算出收税3200
    }
}

/**
 * 收入
 */
class Income {
    protected double income;

    public Income(double income) {
        this.income = income;
    }
    public double getTax() {
        return income * 0.1;
    }
}

/**
 * 工资类
 * 超过5000收税,收多余5000的部分的百分之二十
 * 不超过5000不收税
 */
class Salary extends Income {
    public Salary (double income) {
        super(income);
    }

    @Override
    public double getTax() {
        if (income <= 5000) {
            return 0;
        }
        return (income - 5000) * 0.2;
    }
}
/**
 * 稿费收入
 * 税收是20%
 */
class RoyaltyIncome extends Income {
    public RoyaltyIncome (double income){
        super(income);
    }

    @Override
    public double getTax() {
        return this.income * 0.2;
    }
}