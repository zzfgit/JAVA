/**
 * AbstractTest
 */
public class AbstractTest {

    public static void main(String[] args) {
        AbstractIncome[] incomes = new AbstractIncome[] { new AbstractSalaryIncome(7000),
                new AbstractRoyaltyIncome(12000) };
        double total = 0;
        for (AbstractIncome income : incomes) {
            total = total + income.getTax();
        }
        System.out.println("抽象类中计算出的税是:" + total);
    }
}

/**
 * 计税的抽象类
 */
abstract class AbstractIncome {
    public double income;

    public AbstractIncome(double income) {
        this.income = income;
    }

    public abstract double getTax();
}
/**
 * 工资计税类
 */
class AbstractSalaryIncome extends AbstractIncome {

    public AbstractSalaryIncome(double income) {
        super(income);
    }

    @Override
    public double getTax() {
        if (this.income > 5000) {
            return (this.income - 5000) * 0.1;
        }
        return 0;
    }
}
/**
 * 稿费计税类
 */
class AbstractRoyaltyIncome extends AbstractIncome {

    public AbstractRoyaltyIncome(double income) {
        super(income);
    }

    @Override
    public double getTax() {
        return this.income * 0.2;
    }
}