/**
 * UpClass
 * 向上转型,子类对象指向父类型
 */
public class UpClass {

    public static void main(String[] args) {
        Student4 xiao4 = new Student4("xiao4", 4, 90);
        Person4 p4 = new Student4("p4", 4, 99);//把一个子类对象指向父类类型(向上转型)
        System.out.println(xiao4.name);
        System.out.println(p4.name);

        //将父类对象指向子类类型是不被允许的,因为子类有比父类多的功能,但是父类初始化出的对象不具备这些功能,会报错 ClassCastException
        // Student4 da4 = new Person4("da4", 99);
        // System.out.println(da4.name);

        // 为避免转出错,Java提供了instanceof操作符,可以确定一个实例是不是某种类型
        Person4 p5 = new Person4("p5", 5);
        System.out.println(p5 instanceof Person4); //true
        System.out.println(p5 instanceof Student4); //false

        Student4 xiao5 = new Student4("xiao5", 5, 50);
        System.out.println(xiao5 instanceof Student4);//true
        System.out.println(xiao5 instanceof Person4);//true

        Student4 n = null;
        System.out.println(n instanceof Student4); //false
        // instanceof实际上判断一个变量所指向的实例是否是指定类型，或者这个类型的子类。如果一个引用变量为null，那么对任何instanceof的判断都为false。
        
    }
}