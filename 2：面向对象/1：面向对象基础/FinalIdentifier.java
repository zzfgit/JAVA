/**
 * FinalIdentifier
 * final关键字
 * 集成可以允许子类覆写父类的方法,如果一个父类不允许子类对他的某一个方法进行覆写,可以把该方法标记为final。用final修饰的方法不能被Override：
 */
public class FinalIdentifier {

    public static void main(String[] args) {
        StudentFinal xiaoli = new StudentFinal();
        xiaoli.name = "xiaoli";
        xiaoli.hello();
        System.out.println(xiaoli.number);
        // 对final修饰的属性重新赋值会报错
        // 报错:The final field PersonFinal.number cannot be assigned
        // xiaoli.number = 5;
    }
}

class PersonFinal {
    String name;
    final int number = 3; //使用final修饰的属性,不可重新赋值,但是可以在初始化方法中赋值
    final void hello() {
        System.out.println("Hello " + this.name);
    }

}

final class StudentFinal extends PersonFinal {
    // 在这里就不能覆写父类的hello方法了,因为hello方法用final修饰了
    // @Override
    // public void hello() {
    //     System.out.println(this.name + "Student");
    //     super.hello();
    // }
}

// 如果一个类不希望任何其他类继承自它，那么可以把这个类本身标记为final。用final修饰的类不能被继承
// 下边的Pupil不能继承自StudentFinal,因为StudentFinal类用final修饰了
// 报错:The type Pupil cannot subclass the final class StudentFinal
// class Pupil extends StudentFinal {

// }


/**
 * 小结:
 * 子类可以覆写父类的方法（Override），覆写在子类中改变了父类方法的行为；
Java的方法调用总是作用于运行期对象的实际类型，这种行为称为多态；
final修饰符有多种作用：
final修饰的方法可以阻止被覆写；
final修饰的class可以阻止被继承；
final修饰的field必须在创建对象时初始化，随后不可修改。
 */