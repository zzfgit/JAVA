/**
 * override
 * 重载:
 * 在一个类中，我们可以定义多个方法。如果有一系列方法，它们的功能都是类似的，只有参数有所不同，那么，可以把这一组方法名做成同名方法
 */
public class override {

    public static void main(String[] args) {
        Hello h = new Hello();
        h.hello();
        h.hello("张三");
        h.hello("小王", 13);

        String s = "Test string";
        int n1 = s.indexOf("t");
        int n2 = s.indexOf("st");
        int n3 = s.indexOf("st", 3);

        System.out.println(n1);
        System.out.println(n2);
        System.out.println(n3);

        Person3 xiaoMing = new Person3("xiao", "Ming");
        System.out.println("xiaoMing.name: " + xiaoMing.getName());
    }
}

/**
 * 这种方法名相同，但各自的参数不同，称为方法重载（Overload）。
 * 注意：方法重载的返回值类型通常都是相同的。
 * 方法重载的目的是，功能类似的方法使用同一名字，更容易记住，因此，调用起来更简单。
 */
class Hello{
    public void hello() {
        System.out.println("hello world");
    }
    public void hello(String name) {
        System.out.println("hello " + name);
    }
    public void hello(String name, int age) {
        if (age < 18) {
            System.out.println("Hi, " + name + "!");
        }else{
            System.out.println("Hello, " + name + "!");
        }
    }
}

// 练习:给Person增加重载方法setName(String, String):
class Person3{
    private String name;

    public Person3(String name1, String name2){
        this.name = name1 + " " + name2;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
}