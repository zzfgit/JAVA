/**
 * Interface 在抽象类中，抽象方法本质上是定义接口规范：即规定高层类的接口，从而保证所有子类都有相同的接口实现，这样，多态就能发挥出威力。
 * 
 * 如果一个抽象类没有字段，所有方法全部都是抽象方法：
 * 
 * abstract class Person { public abstract void run(); public abstract String
 * getName(); } 就可以把该抽象类改写为接口：interface
 */
public class Interface {

    public static void main(String[] args) {
        System.out.println("接口");
    }
}

// 所谓interface，就是比抽象类还要抽象的纯抽象接口，因为它连字段都不能有。因为接口定义的所有方法默认都是public abstract的，所以这两个修饰符不需要写出来（写不写效果都一样）。
interface InterfacePerson{
    void run();
    String getName();
}

interface InterfaceHello{
    void hello();
}

// 当一个具体的class去实现一个interface时，需要使用implements关键字
class ImplementStudent implements InterfacePerson {
    private String name;

    public ImplementStudent(String name){
        this.name = name;
    }
    @Override
    public void run() {
        System.out.println(this.name + " run");
    }
    @Override
    public String getName() {
        return this.name;
    }
}

// 在Java中,一个类只能继承自另一个类,不能从多个类继承,但是,一个类可以实现多个interface
// 实现了 InterfacePerson,InterfaceHello 两个接口
class ImplementStudent1 implements InterfacePerson, InterfaceHello {
    private String name;
    @Override
    public void run() {
        System.out.println(this.name + "run");
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public void hello() {
        System.out.println("Hello");
    }

}

// 接口继承
// 一个interface可以继承自另一个interface。interface继承自interface使用extends，它相当于扩展了接口的方法
interface InterfaceStudent extends InterfacePerson{
    void study();
}
// InterfaceStudent这个接口继承自 InterfacePerson,所以InterfaceStudent实际上有3个抽象方法签名,有两个来自InterfacePerson