/**
 * ParamBind
 */
public class ParamBind {

    public static void main(String[] args) {

        // 1:基本类型参数绑定
        Person p = new Person();
        int n = 12;
        p.setAge(n);
        System.out.println(p.getAge());

        // 改变外部变量n,不影响p的age字段,因为setAge方法获得了参数,复制了n的值,因此p.age和n互不影响
        n = 20;
        System.out.println(p.getAge());

        // 2:引用类型参数绑定
        // 引用类型的参数传递进去的是数组的引用地址,所以外部改变数组的内容,对象p2的name属性引用的数组也会跟着改变
        Person p2 = new Person();
        String[] fullname = new String[] {"homer", "simpson"};
        p2.setName(fullname);
        System.out.println(p2.getName()[0]);
        fullname[0] = "bart";
        System.out.println(p2.getName()[0]);

        // 3:值传递,两次都打印出boos
        Person p3 = new Person();
        String title = "boos";
        p3.setTitle(title);
        System.out.println(p3.getTitle());//boos

        title = "manager";
        System.out.println(p3.getTitle());//boos
    }
}

/**
 * Person
 */
class Person {

    private int age;

    private String[] name;

    private String title;
    
    public int getAge() {
        return this.age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public String[] getName() {
        return this.name;
    }
    public void setName(String[] name) {
        this.name = name;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}