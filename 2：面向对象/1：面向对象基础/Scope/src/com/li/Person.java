package com.li;

import com.zuo.Hello;

public class Person {
    public static void main(String[] args) {

        //这里可以访问Hello类中的public修饰的hi方法
        Hello hello = new Hello();
        hello.hi();

    }
}
