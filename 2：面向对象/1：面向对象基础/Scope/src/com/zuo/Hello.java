package com.zuo;

import sun.applet.Main;

/*
* 作用域
* 在Java中，我们经常看到public、protected、private这些修饰符。在Java中，这些修饰符可以用来限定访问作用域。
* */
public class Hello {
    public static void main(String[] args) {

    }
    //定义为public的 class，interface 可以被其他任何类访问
    public void hi() {
        System.out.println("hi");
    }

    //定义为private 的filed , method无法被其他类访问
    private void hello(){
        this.hi();
    }

    private static void hello1(){
        System.out.println("私有静态方法");
    }

    //因为Java支持嵌套类，如果一个类内部定义了嵌套类，那么嵌套类可以访问private修饰的属性和方法
    static class inner {
        public void hi(){
            Hello.hello1();
        }
    }
}

//使用final修饰的类无法被继承
final class HelloFinal {
    private int n = 0;
    protected void hi(int t){
    }
}

//这里会报错，无法继承自HelloFinal
//class HelloFinalSub extends HelloFinal {
//
//}
