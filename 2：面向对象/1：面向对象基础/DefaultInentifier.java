/**
 * DefaultInentifier
 * default方法
 * 
 */
public class DefaultInentifier {

    public static void main(String[] args) {
        Student xiaoli = new Student("xiali");
        xiaoli.eat();
        xiaoli.run();
    }
}

/**
 * Person
 * 实现类可以不必覆写default方法。default方法的目的是，当我们需要给接口新增一个方法时，会涉及到修改全部子类。如果新增的是default方法，那么子类就不必全部修改，只需要在需要覆写的地方去覆写新增方法。
 */
interface PersonInterface {

    String getName();
    default void run(){
        System.out.println(getName() + " run");
    }
    void eat();
}

/**
 * Student 
 */
class Student implements PersonInterface {

    private String name;
    public Student(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    @Override
    public void eat() {
        System.out.println(this.name + " eat");
    }
}