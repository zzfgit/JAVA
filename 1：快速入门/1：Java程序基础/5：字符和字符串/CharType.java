/**
 * CharType
 * 字符类型
 * 字符类型char是基本数据类型，它是character的缩写。一个char保存一个Unicode字符：
 */
public class CharType {

    public static void main(String[] args) {
        CharType ct = new CharType();
        ct.defineChar();
    }
    private void defineChar() {
        char c1 = 'A';
        char c2 = 'a';
        System.out.println(c1);
        System.out.println(c2);
        // 因为Java在内存中总是使用Unicode表示字符，所以，一个英文字符和一个中文字符都用一个char类型表示，它们都占用两个字节。要显示一个字符的Unicode编码，只需将char类型直接赋值给int类型即可：
        int i1 = c1;
        int i2 = c2;
        System.out.println(i1); //65
        System.out.println(i2); //97
    }
}