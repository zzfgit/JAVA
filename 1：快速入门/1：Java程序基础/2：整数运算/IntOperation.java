/**
 * IntOperation
 */
public class IntOperation {

    public static void main(String[] args) {
        int i = (100 + 200) * (99 -88);
        System.out.println(i);

        int x = 123456 / 78;
        System.out.println(x);

        //求余数使用 % 
        int y = 12345 % 67;
        System.out.println(y);

       IntOperation io = new IntOperation();
       io.typeManuallyTransfer();

       io.sum(10000);
    }
    public void typeAutoTransfer() {
        // 在运算过程中，如果参与运算的两个数类型不一致，那么计算结果为较大类型的整型。例如，short和int计算，结果总是int，原因是short首先自动被转型为int：
        short s = 1234;
        int i1 = 123456;
        int is = s + i1; //s将会自动转换为int类型进行运算
        // short si = s + i;//编译错误,无法将int类型转换为short类型
        System.out.println(is);
    }

    public void typeManuallyTransfer() {
        // 也可以将结果强制转型，即将大范围的整数转型为小范围的整数。强制转型使用(类型)，例如，将int强制转型为short：
        int i2 = 1234;
        short s2 = (short)i2; //将int类型的i2转换成short类型
        System.out.println(s2);

        // 要注意，超出范围的强制转型会得到错误的结果，原因是转型时，int的两个高位字节直接被扔掉，仅保留了低位的两个字节：
        int i3 = 1234567;
        short s3 = (short)i3;//强转导致结果变化
        System.out.println(s3);
    }

    // 练习题,计算前n个自然数的和
    public void sum(int n) {
        int result = (1 + n) * n /2;
        System.out.printf("前%d个自然数的和是:%d%n",n,result);
    }
}