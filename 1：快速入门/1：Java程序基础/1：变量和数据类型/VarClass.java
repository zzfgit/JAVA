import jdk.nashorn.internal.ir.Assignment;

/**
 * varClass
 */
public class varClass {

    public static void main(String[] args) {
        varClass v1 = new varClass();
        v1.assignment1();
    }
    //变量赋值与重新赋值
    public void assignment() {
        int x = 100;//定义变量x,复制100;
        System.out.println(x);
        x = 200;    //重新赋值
        System.out.println(x);
    }

    // 用其他变量给变量赋值
    public void assignment1() {
        int n = 100;
        System.out.println("n = " + n); //打印n的值

        n = 200;//变量n重新赋值200
        System.out.println("n = " + n);//打印n的值

        int x = n;//变量x赋值为n(n的值为200)
        System.out.println("x = " + x);

        x = x + 100;
        System.out.println("x = " + x);
        System.out.println("n = " + n);
    }
}