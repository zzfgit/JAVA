/**
 * finalType
 */
public class finalType {

    public static void main(String[] args) {
        //定义变量的时候,如果加上final修饰符,这个变量就变成了常量
        //常量在定义时进行初始化后就不能再次赋值,再次赋值会编译错误
        final double D = 3.14; //d是一个常量
        //d = 3.15; 重新赋值会报错
        System.out.println(D);
    }
}