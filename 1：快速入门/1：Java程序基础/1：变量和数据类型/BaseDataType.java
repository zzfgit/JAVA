/**
 * baseDataType
 */
public class baseDataType {

    public static void main(String[] args) {
        baseDataType intType = new baseDataType();
        intType.defineBool();
    }

    // 整型类型
    public void defineInt() {
        int i = 2147483647;//整型的最大值
        int i2 = -2147483647;//整型的最小值

        System.out.println(i);
        System.out.println(i2);
    }

    // 浮点类型
    public void defineFloat() {
        float f1 = 3.14f;//浮点数后边需要加f
        float f2 = 3.14e38f;//科学计数法表示3.14*10^38
        double d1 = 1.79e308;
        double d2 = -1.79e308;
        double d3 = 4.9e-324;//科学计数法标识 4.9*10^324

        System.out.println("f1 = " +f1);
        System.out.println("f2 = " +f2);
        System.out.println("d1 = " +d1);
        System.out.println("d2 = " +d2);
        System.out.println("d3 = " +d3);
    }

    // 布尔类型,只有true和false两个值
    public void defineBool() {
        boolean b1 = true;
        boolean b2 = false;
        boolean isGreater = 5 > 3;
        int age = 12;
        boolean isAdult = age >= 18;

        System.out.println("isGrenter = " + isGreater);
        System.out.println("isAdult = " + isAdult);
        System.out.println(b1);
        System.out.println(b2);
    }
}