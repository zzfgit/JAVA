/**
 * varIdentifier
 */
public class varIdentifier {

    public static void main(String[] args) {
        //使用var关键字定义变量,编译器会根据后边的赋值语句自动推断变量的类型
        var sb = new StringBuilder();
        //编译器会根据赋值语句自动推断sb是StringBuilder类型
        //因此使用var定义变量,仅仅是少些了变量类型
        
    }
}