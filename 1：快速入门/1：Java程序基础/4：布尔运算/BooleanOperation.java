/**
 * BooleanOperation
 * 对于boolean类型,只有两个值 true 和 false
 * 比较运算符: > ,>=, <, <=, ==, !=
 * 与运算 &&
 * 或运算 ||
 * 非运算 !
 */
public class BooleanOperation {

    public static void main(String[] args) {
        BooleanOperation bo = new BooleanOperation();
        bo.booleanDemo();
        bo.ternaryOperator();
        bo.test(8);
    }
    private void booleanDemo() {
        boolean isGreater = 5 > 3;//true
        int age = 12;
        boolean isZero = age == 0;//false
        boolean isNonZero = !isZero;//true
        boolean isAdult = age >= 18;//false
        boolean isTeenager = age > 6 && age < 18;//true

        System.out.println(isGreater);
        System.out.println(isZero);
        System.out.println(isNonZero);
        System.out.println(isAdult);
        System.out.println(isTeenager);
    }

    //三元运算符,根据第一个表达式的结果,分别返回后续的两个表达式之一的计算结果
    private void ternaryOperator() {
        int n = -100;
        int x = n >= 0 ? n : -n;//n大于等于0的话返回n,否则返回n的负数,这是一个求绝对值的表达式
        System.out.println(x);
    }

    //练习题:判断这个年龄是不是小学生 (6-12岁之间)
    private void test(int age) {
        boolean isPrimaryStudent = age >= 6 && age <= 12;
        System.out.println(isPrimaryStudent ? "是小学生" : "不是小学生");
    }
}