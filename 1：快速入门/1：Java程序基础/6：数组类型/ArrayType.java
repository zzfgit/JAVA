import java.lang.reflect.Method;

/**
 * ArrayType
 */
public class ArrayType {

    public static void main(String[] args) {
        ArrayType at = new ArrayType();
        at.test();
    }

    private void defineArray() {
        // 定义一个数组类型的变量，使用数组类型“类型[]”，例如，int[]。和单个基本类型变量不同，数组变量初始化必须使用new int[5]表示创建一个可容纳5个int元素的数组。
        //定义一个有5个int类型元素的额数组ns
        int[] ns = new int[5];
        ns[0] = 68;
        ns[1] = 79;
        ns[2] = 91;
        ns[3] = 85;
        ns[4] = 62;
        System.out.println(ns[3]);
        //java数组的几个特点:
        //1 数组所有元素初始化为默认值，整型都是0，浮点型是0.0，布尔型是false；
        //2 数组一旦创建后，大小就不可改变。

        // 要访问数组中的某一个元素，需要使用索引。数组索引从0开始，例如，5个元素的数组，索引范围是0~4。
        // 可以修改数组中的某一个元素，使用赋值语句，例如，ns[1] = 79;。
        // 可以用数组变量.length获取数组大小
        ns[1] = 100;
        System.out.println(ns[1]);
        System.out.println("ns数组的长度是:" + ns.length);

        //也可以在定义数组时直接指定初始化的元素，这样就不必写出数组大小，而是由编译器自动推算数组大小
        int[] ns1 = new int[] {1,2,3,4,5};
        System.out.println(ns1.length);

        // 可以进一步简写
        int[] ns2 = {1,2,3,4,5};
        System.out.println(ns2.length);
    }
    private void arrayNoChange() {
        // 注意数组是引用类型，并且数组大小不可变
        int[] ns;
        ns = new int[] {1,2,3,4,5};
        System.out.println("ns数组的长度" + ns.length); //5
        ns = new int [] {1,2,3};
        System.out.println("ns数组的长度" + ns.length); //3

        //看起来ns数组变了,其实没变
        // ns只是指向了一个新的3个元素的数组,原来的5个元素的数组没有变,只是无法通过ns引用到而已.
    }

    private void stringArray() {
        String[] names = {
            "abc","xyz","zoo"
        };
        System.out.println("names的第一个元素是:" + names[0]);
        names[0] = "cat";
        System.out.println("修改后names的第一个元素是:" + names[0]);

        // names的第一个元素修改后,names[0]指向了一个新的字符串"cat",原来的内存位置的"abc"依然没变,只是没有办法通过数组元素引用它了.
    }

    private void test() {
        String[] names = {"abc","aaa","zoo"};
        String s = names[1];
        names[1] = "cat";
        System.out.println(s); //aaa,s指向的字符串没变,一直是aaa,知识names[1]变了而已.
    }
}