/**
 * FloatOperation
 * 浮点数运算和整数运算相比,只能进行加减乘除这些数值运算,不能做位运算和移位运算
 * 浮点数的表示范围很大,但是浮点数有个非常重要的特点,即使常常无法精确标识
 * 浮点数0.1在计算机中就无法精确标识因为十进制的0.1换算成二进制是一个无限循环小数，很显然，无论使用float还是double，都只能存储一个0.1的近似值。但是，0.5这个浮点数又可以精确地表示。
 * 因为浮点数常常无法精确表示，因此，浮点数运算会产生误差：
 */
public class FloatOperation {

    public static void main(String[] args) {
        FloatOperation fo = new FloatOperation();
        // fo.double1();
        // fo.doubleCompare();
        fo.test();
    }
    public void double1() {
        double x = 1.0 / 10;
        double y = 1 - 9.0 /10;

        //两个结果不一样
        System.out.println(x);
        System.out.println(y);
    }

    // 由于浮点数存在运算误差，所以比较两个浮点数是否相等常常会出现错误的结果。正确的比较方法是判断两个浮点数之差的绝对值是否小于一个很小的数：
    public void doubleCompare() {
        double x = 1.0 / 10;
        double y = 1 - 9.0 /10;
    // 比较x和y是否相等，先计算其差的绝对值:
        double r = Math.abs(x - y);
        if(r < 0.00000009){
            System.out.println("相等");
        }else{
            System.out.println("不相等");
        }
    }

    //练习题:
    public void test() {
        double a = 1.0;
        double b = 3.0;
        double c = -4.0;

        double r1 = 0;
        double r2 = 0;
        r1 = (-b + Math.sqrt(b * b - 4 * a * c)) / 2 * a;
        r2 = (-b - Math.sqrt(b * b - 4 * a * c)) / 2 * a;

        System.out.println("r1:"+ r1);
        System.out.println("r2" + r2);
    }
}