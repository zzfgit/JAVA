/**
 * WhileLoop
 * while循环在每次循环开始前，首先判断条件是否成立。如果计算结果为true，就把循环体内的语句执行一遍，如果计算结果为false，那就直接跳到while循环的末尾，继续往下执行。
 */
public class WhileLoop {

    public static void main(String[] args) {
        WhileLoop wl = new WhileLoop();
        wl.test(10, 20);
        wl.doWhileDemo();
        wl.test1(0, 100);
    }

    private void whileDemo() {
        int sum = 0;
        int n = 1;
        while (n<=100) {
            sum = sum + n;
            n++;
        }
        System.out.println(sum);
    }

    // 练习 1:使用while计算从m到n的和
    private void test(int m,int n) {
        int sum = 0;
        while (m <= n) {
            sum += m;
            m++;
        }
        System.out.printf("%d 到 %d 的和是:%d \n",m,n,sum);
    }

    // do while
    // 在Java中，while循环是先判断循环条件，再执行循环。而另一种do while循环则是先执行循环，再判断条件，条件满足时继续循环，条件不满足时退出
    private void doWhileDemo() {
        int sum = 0;
        int n = 1;
        do {
            sum += n;
            n ++;   
        } while (n<=100);
        System.out.println(sum);
    }

    // 练习2 :使用do while 循环,计算从m到n的和
    private void test1(int m,int n) {
        int sum = 0;
        do {
            sum += m;
            m ++;
        } while (m <= n);
        System.out.println("练习二结果:" + sum);
    }
}