/**
 * Break
 * 在循环过程中，可以使用break语句跳出当前循环
 */
public class Break {

    public static void main(String[] args) {
        Break bk = new Break();
        bk.breakTag();
    }

    // 1:break
    private void breakDemo() {
        int sum = 0;
        for (int i = 0;; i++) {
            sum = sum + i;
            if (i == 100) {
                break;//当i等于100时,就退出循环
            }
        }
        System.out.println(sum);
    }

    // 2:continue
    // break会跳出当前循环，也就是整个循环都不会执行了。而continue则是提前结束本次循环，直接继续执行下次循环
    private void continueDemo() {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            System.out.println("begin i = " + i);
            if (i % 2 == 0) {
                continue;
            }
            sum = sum + 1;
            System.out.println("end i = " + i);
        }
        System.out.println(sum);
    }

    // 3:break 标记:跳出多层循环
    private void breakTag() {
        for (int i = 0; i < 10; i++) {
            OUT://在此处打一个标记:OUT,后边直接在break后边加标记,就可以退出到这个标记处
            for (int a = 0; a < 10; a++) {
                for (int b = 0; b < 10; b++) {
                    for (int c = 0; c < 10; c++) {
                        for (int d = 0; d < 10; d++) {
                            System.out.println("d = " + d);
                            if (d == 3) {
                                break OUT;
                            }
                        }
                    }
                }
            }
        }
    }
}