import java.util.Scanner;
/**
 * InAndOut
 */
public class InAndOut {

    public static void main(String[] args) {
        InAndOut io = new InAndOut();
        io.print1();
        io.formatPrintf1();
        // io.scanner1();
        io.test();
    }

    //1:输出
    //在前面的代码中，我们总是使用System.out.println()来向屏幕输出一些内容。
    // println是print line的缩写，表示输出并换行。因此，如果输出后不想换行，可以用print()：
    private void print1() {
        System.out.print("A,");
        System.out.print("B,");
        System.out.print("C,");
        System.out.println();
        System.out.println("END");
    }

    //2:格式化输出
    /**
     * Java的格式化功能提供了多种占位符，可以把各种数据类型“格式化”成指定的字符串：
     * %d	格式化输出整数
     * %x	格式化输出十六进制整数
     * %f	格式化输出浮点数
     * %e	格式化输出科学计数法表示的浮点数
     * %s	格式化字符串
     */
    private void formatPrintf() {
        double d = 12900000;
        System.out.println(d);//1.29E7

        // 如果要把数据显示成我们期望的格式，就需要使用格式化输出的功能。格式化输出使用System.out.printf()，通过使用占位符%?，printf()可以把后面的参数格式化成指定格式：
        double d1 = 3.1415926;
        System.out.printf("%.2f\n",d1); //显示2位小数 3.14
        System.out.printf("%.4f\n",d1); //显示4位小数 3.1416
    }

    // 占位符本身还可以有更详细的格式化参数。下面的例子把一个整数格式化成十六进制，并用0补足8位
    private void formatPrintf1() {
        int n = 123456000;
        System.out.printf("n = %d , hex = %08x \n", n , n);
    }

    //3:输入
    private void scanner1() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input you bname: ");
        String name = scanner.nextLine();

        System.out.print("Input you age");
        int age = scanner.nextInt();

        System.out.printf("Hi,%s,you are %d\n", name,age);
        scanner.close();
    }

    //4:练习
    private void test() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input you lastTime result: ");
        int lastTimeResult = scanner.nextInt();

        System.out.print("Input you this result: ");
        int thisResult = scanner.nextInt();

        scanner.close();

        float f = (thisResult - lastTimeResult) / (float)lastTimeResult;
        System.out.printf("本次成绩相比上次提高了%.2f%% \n", f  * 100);
    }
}