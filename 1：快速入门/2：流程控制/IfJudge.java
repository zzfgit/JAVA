import java.util.Scanner;

/**
 * IfJudge
 * 在Java程序中，如果要根据条件来决定是否执行某一段代码，就需要if语句。
 */
public class IfJudge {

    public static void main(String[] args) {
        IfJudge ij = new IfJudge();
        ij.bmi();
    }
    // 1:if判断例子
    private void ifDemo() {
        int n = 70;
        if (n >= 60) {
            System.out.println("及格了");
        }
        System.out.println("END");
    }
    // 2:当if语句块只有一行时,可以省略 {}
    private void ifDemo1() {
        int n = 70;
        if (n >= 60) 
            System.out.println("及格了");
        System.out.println("END");
    }
    // 3:if语句还可以编写一个else { ... }，当条件判断为false时，将执行else的语句块：
    private void ifElseDemo() {
        int n = 70;
        if (n >= 60) {
            System.out.println("恭喜你,及格了!");
        } else {
            System.out.println("你挂了!");
        }
        System.out.println("END");
    }

    // 4:判断引用类型相等
    // 在Java中，判断值类型的变量是否相等，可以使用==运算符。但是，判断引用类型的变量是否相等，==表示“引用是否相等”，或者说，是否指向同一个对象。例如，下面的两个String类型，它们的内容是相同的，但是，分别指向不同的对象，用==判断，结果为false：
    private void referenceJudge() {
        String s1 = "hello";
        String s2 = "HELLO".toLowerCase();
        System.out.println(s1);
        System.out.println(s2);
        if (s1 == s2) {
            System.out.println("s1 ==  s2");
        }else{
            System.out.println("s1 != s2");
        }
    }

    // 5: 要判断引用类型的变量内容是否相等，必须使用equals()方法：
    private void referenceJudge1() {
        String s1 = "hello";
        String s2 = "HELLO".toLowerCase();
        System.out.println(s1);
        System.out.println(s2);
        if (s1.equals(s2)) {
            System.out.println("s1 equals s2");
        }else{
            System.out.println("s1 not equals s2");
        }
    }
    // 6:练习
    // 请用if ... else编写一个程序，用于计算体质指数BMI，并打印结果。

    // BMI = 体重(kg)除以身高(m)的平方
    // BMI结果：
    // 过轻：低于18.5
    // 正常：18.5-25
    // 过重：25-28
    // 肥胖：28-32
    // 非常肥胖：高于32
    private void bmi() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入您的身高(cm):");
        int height = scanner.nextInt();

        System.out.println("请输入您的体重(kg):");
        int weight = scanner.nextInt();

        scanner.close();

        float heightCm = (float)height / 100;
        float bmiValue = weight / (heightCm * heightCm);
        System.out.println("bmi值为:" + bmiValue);
        if (bmiValue < 18.5) {
            System.out.println("过轻");
        }else if (bmiValue < 25) {
            System.out.println("正常");
        }else if (bmiValue < 28) {
            System.out.println("过重");
        }else if (bmiValue < 32) {
            System.out.println("肥胖");
        }else if (bmiValue > 32) {
            System.out.println("非常肥胖");
        }
    }
}