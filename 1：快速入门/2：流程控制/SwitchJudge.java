import java.util.Random;
import java.util.Scanner;

/**
 * SwitchJudge
 */
public class SwitchJudge {

    public static void main(String[] args) {
        SwitchJudge sj = new SwitchJudge();
        sj.game();
    }

    //1: switch
    private void switchDemo() {
        int option = 1;
        switch (option) {
            case 1:
                    System.out.println("Selected 1");
                break;
            case 2:
                    System.out.println("Selected 2");
                break;
            case 3:
                    System.out.println("Selected 3");
                break;
            default:
                    System.out.println("No Selected");
                break;
        }
    }

    // 2:switch还可以匹配字符串,字符串匹配时,是比较"内容相等"
    private void switchDemo1() {
        String fruit = "apple";
        switch (fruit) {
            case "apple":{
                System.out.println("苹果");
            }
                break;
            case "pear":{
                System.out.println("梨");
            }
                break;
            case "mango":{
                System.out.println("芒果");
            }
                break;
            default:
            {
                System.out.println("啥也不是");
            }
                break;
        }
    }

    // // 3:switch不需要break,需要Java12之后的版本
    // private void noBreakSwitch() {
    //     String fruit = "apple";
    //     switch (fruit) {
    //         case "apple" -> System.out.println("苹果");
    //         case "orange" -> System.out.println("橘子");
    //         case "mango" -> System.out.println("芒果");
        
    //         default:
    //             break;
    //     }
    // }
   
    // 3:练习
    // 石头剪刀布游戏
    private void game() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("石头剪刀布游戏开始:");
        System.out.println("您要出什么? 1:石头 2:剪刀 3:布");

        int player = scanner.nextInt();
        int computer = (int)(1 + Math.random()*3);

        scanner.close();

        System.out.println("您出了" + player);
        System.out.println("电脑出了" + computer);

        int result = player - computer;
        switch (result) {
            case -1:
            case 2:
                System.out.println("你赢了!");
                break;
            case 0:
                System.out.println("平局");
                break;
            default:
                System.out.println("你输了");
                break;
        }

    }
}
