import java.util.Arrays;

/**
 * ArraySort
 */
public class ArraySort {

    public static void main(String[] args) {
        ArraySort as = new ArraySort();
        int[] ns = {3,23,34,54,15,26,7,38,69,10};
        as.maoPaoSort(ns);

        as.test(ns);
    }

    // 1:冒泡排序
    private void maoPaoSort(int[] ns) {
        for (int i = 0; i < ns.length - 1; i++) {
            for(int j = 0 ; j < ns.length - i - 1; j++){
                if (ns[j] > ns[j + 1]) {
                    //交换ns[j]和ns[j+1]
                    int tmp = ns[j];
                    ns[j] = ns[j + 1];
                    ns[j + 1] = tmp;
                }
            }
        }
        //排序后
        System.out.println(Arrays.toString(ns));
    }

    //注:其实Java的标准库已经内置了排序功能,只需要调用JDK提供的Arrays.sort()就可以对数组进行排序


    // 2:练习
    // 对数组进行降序排列
    private void test(int[] ns) {
        for (int i = 0; i < ns.length - 1; i++) {
            for(int j = 0 ; j < ns.length - i - 1; j++){
                if (ns[j] < ns[j + 1]) {
                    //交换ns[j]和ns[j+1]
                    int tmp = ns[j];
                    ns[j] = ns[j + 1];
                    ns[j + 1] = tmp;
                }
            }
        }
        //排序后
        System.out.println(Arrays.toString(ns));
    }
}