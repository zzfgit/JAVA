import java.lang.reflect.Array;

/**
 * ArrayErgodic
 */
public class ArrayErgodic {

    public static void main(String[] args) {
        ArrayErgodic ae = new ArrayErgodic();
        int[] ns = {1,2,3,4,5,6,7,8,9,10};
        ae.invertedPrint(ns);
    }

    // 1:使用for循环遍历
    private void arrayErgodicDemo1() {
        int []ns = {1,2,3,4,5};
        for (int i = 0; i < ns.length; i++) {
            int n = ns[i];
            System.out.println(n);
        }
    }

    // 2:使用for each 遍历
    private void arrayErgodicDemo2() {
        int []ns = {1,2,3,4,5};
        for (int n : ns) {
            System.out.println(n);
        }
    }
    // 3:直接打印数组内容
    // 使用Array.toString()函数
    // private void arrayToString() {
    //     // int[] ns = {1,2,3,4,5};
    //     // System.out.println(Arrays.toString(ns));
    //     int[] ns = { 1, 1, 2, 3, 5, 8 };
    //     System.out.println(Arrays.toString(ns));
    // }

    // 4:练习:倒序打印数组的每个元素
    private void invertedPrint(int[] ns) {
        for (int i = ns.length - 1; i >= 0; i--) {
            System.out.println(ns[i]);
        }
    }
}