package com.assertpackage;
/*
* 断言
* 断言（Assertion）是一种调试程序的方式。在Java中，使用assert关键字来实现断言。
* Java断言的特点是：断言失败时会抛出AssertionError，导致程序结束退出。因此，断言不能用于可恢复的程序错误，只应该用于开发和测试阶段。
* */
public class Assert {
    public static void main(String[] args) {
        assertDemo();
    }

    //语句assert x >= 0;即为断言，断言条件x >= 0预期为true。如果计算结果为false，则断言失败，抛出AssertionError。
    static void assertDemo(){
        double x = Math.abs(-123.45);
        assert x >= 0 : "x must >= 0";
        System.out.println(x);
    }
}
