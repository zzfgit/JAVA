package exceptional;

/*
* 自定义异常
* 在一个大型项目中，可以自定义新的异常类型，但是，保持一个合理的异常继承体系是非常重要的。
* 一个常见的做法是自定义一个BaseException作为“根异常”，然后，派生出各种业务类型的异常
*
*
* */
public class CustomException {
    public static void main(String[] args) {

        try {
            String s = login("admi","password");
            System.out.println(s);
        }catch (UserNotFoundException e){
            e.printStackTrace();
        }catch (LoginFailedException e){
            e.printStackTrace();
        }

        System.out.println("之后的代码");
    }

    static String login(String userName,String password){
        if (userName.equals("admin")){
            if (password.equals("password")){
                return "登录成功";
            }else{
                //抛出LoginFailedException:
                throw new LoginFailedException("密码错误");
            }
        }else{
            //抛出用户未找到异常
            throw new UserNotFoundException("用户不存在");
        }
    }
}

//自定义异常基类
class BaseException extends RuntimeException{
    public BaseException(){
        super();
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }
}

//异常派生类:用户不存在异常
class UserNotFoundException extends BaseException {
    public UserNotFoundException(String userName){
        super(userName);
    }

}

//异常派生类,登录失败异常
class LoginFailedException extends BaseException {
    public LoginFailedException(String password){
        super(password);
    }
}
