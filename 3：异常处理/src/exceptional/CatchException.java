package exceptional;
/*
 * 捕获异常
 *在Java中，凡是可能抛出异常的语句，都可以用try ... catch捕获。把可能发生异常的语句放在try { ... }中，然后使用catch捕获对应的Exception及其子类。
 * */

public class CatchException {
    public static void main(String[] args) {

        //练习
        String a = "12";
        String b = "x9";

        //TODO:捕获异常并处理
        try {
            int c = stringToInt(a);
            int d = stringToInt(b);
            System.out.println(c * d);

        }catch (NumberFormatException e){
            System.out.println("错误,字符串非全数字,无法转换");
        }finally {
            System.out.println("END");
        }
    }
    //1:多catch语句
    //可以使用多个catch语句，每个catch分别捕获对应的Exception及其子类。JVM在捕获到异常后，会从上到下匹配catch语句，匹配到某个catch后，执行catch代码块，然后不再继续匹配。
    /*
    * try {
        process1();
        process2();
        process3();
    } catch (IOException e) {
        System.out.println(e);
    } catch (NumberFormatException e) {
        System.out.println(e);
    }*/
    //存在多个catch的时候，catch的顺序非常重要：子类必须写在前面。
    /*
    * 错误示范
    * try {
        process1();
        process2();
        process3();
    } catch (IOException e) {
        System.out.println("IO error");
    } catch (UnsupportedEncodingException e) { // 永远捕获不到
        System.out.println("Bad encoding");
    }
    * 对于上面的代码，UnsupportedEncodingException异常是永远捕获不到的，因为它是IOException的子类。当抛出UnsupportedEncodingException异常时，会被catch (IOException e) { ... }捕获并执行*/
    //因此,正确的写法是把异常的子类放到前边
    /*
    * try {
        process1();
        process2();
        process3();
    } catch (UnsupportedEncodingException e) {
        System.out.println("Bad encoding");
    } catch (IOException e) {
        System.out.println("IO error");
    }*/

    //2:finally语句
    //无论是否有异常发生，如果我们都希望执行一些语句，例如清理工作，怎么写
    //Java的try ... catch机制还提供了finally语句，finally语句块保证有无错误都会执行
    /*
    * try {
        process1();
        process2();
        process3();
    } catch (UnsupportedEncodingException e) {
        System.out.println("Bad encoding");
    } catch (IOException e) {
        System.out.println("IO error");
    } finally {
        System.out.println("END");
    }*/
    //1:finally语句不是必须的，可写可不写；
    //2:finally总是最后执行。

    //3:练习:使用try catch捕获异常并处理
    static int stringToInt(String s) throws NumberFormatException{
        return Integer.parseInt(s);
    }

}
