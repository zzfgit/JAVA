package exceptional;
/*
* 捕获异常
* 捕获异常使用try...catch语句，把可能发生异常的代码放到try {...}中，然后使用catch捕获对应的Exception及其子类：
*
* */

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class javaExceptional {
    public static void main(String[] args) {
        byte[] bs = toGBK("中文");
        System.out.println(Arrays.toString(bs));
    }
    static byte[] toGBK(String s){
        try {
//            用指定编码转换String为byte
            System.out.println("支持GBK编码");
            return s.getBytes("GBK");

        }catch (UnsupportedEncodingException e){
//            如果系统不支持GBK编码,会捕获到UnsupportedEncodingException
            System.out.println(e);
            return s.getBytes();
        }
    }
}
