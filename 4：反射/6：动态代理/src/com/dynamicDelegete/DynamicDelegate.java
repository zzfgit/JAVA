package com.dynamicDelegete;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/*
* 动态代理
*
*
我们来比较Java的class和interface的区别：

可以实例化class（非abstract）；
不能实例化interface。
所有interface类型的变量总是通过向上转型并指向某个实例的：
*
*
有没有可能不编写实现类，直接在运行期创建某个interface的实例呢？

这是可能的，因为Java标准库提供了一种动态代理（Dynamic Proxy）的机制：可以在运行期动态创建某个interface的实例。

什么叫运行期动态创建？听起来好像很复杂。所谓动态代理，是和静态相对应的。
* */
public class DynamicDelegate {
    public static void main(String[] args) {
        dynamicCreateInstance();
    }
//    动态代码，我们仍然先定义了接口Hello，但是我们并不去编写实现类，而是直接通过JDK提供的一个Proxy.newProxyInstance()创建了一个Hello接口对象。这种没有实现类但是在运行期动态创建了一个接口对象的方式，我们称为动态代码。JDK提供的动态创建接口对象的方式，就叫动态代理。
    private static void dynamicCreateInstance() {
        InvocationHandler handler = new InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                System.out.println(method);
                if (method.getName().equals("morning")){
                    System.out.println("Good morning");
                }
                return null;
            }
        };
        Hello hello = (Hello) Proxy.newProxyInstance(
                Hello.class.getClassLoader(),// 传入ClassLoader
                new Class[]{Hello.class},// 传入要实现的接口
                handler);// 传入处理调用方法的InvocationHandler
        hello.morning("Bob");
    }
}

interface Hello{
    void morning(String name);
}