package com.getField;
import java.lang.reflect.Field;

/*
*获取字段值
* 利用反射拿到字段的一个Field实例只是第一步，我们还可以拿到一个实例对应的该字段的值。
*  */
public class GetField {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
//        getField();
//        setField();
        test();
    }


    //获取字段值
    static void getField() throws NoSuchFieldException, IllegalAccessException {
        Object p = new Person("xiaoming");
        Class c = p.getClass();
        Field f = c.getDeclaredField("name");
        f.setAccessible(true);//别管这个字段是不是public，一律允许访问
        Object value = f.get(p);
        System.out.println(value);
    }

    //设置字段值
    //通过Field实例既然可以获取到指定实例的字段值，自然也可以设置字段的值。
    //设置字段值是通过Field.set(Object, Object)实现的，其中第一个Object参数是指定的实例，第二个Object参数是待修改的值。
    static void setField() throws NoSuchFieldException, IllegalAccessException {
        Person p = new Person("xiaoMing");
        System.out.println(p.getName());//xiaoMing
        Class c = p.getClass();
        Field f = c.getDeclaredField("name");
        f.setAccessible(true);
        f.set(p,"xiaoZhang");
        System.out.println(p.getName());//xiaoZhang
    }

    //练习:利用反射给name和age字段赋值:
    static void test() throws NoSuchFieldException, IllegalAccessException {
        String name = "Xiao Ming";
        int age = 20;
        PersonTest p = new PersonTest();
        // TODO: 利用反射给name和age字段赋值:

        Class c = p.getClass();
        Field namef = c.getDeclaredField("name");
        namef.setAccessible(true);
        namef.set(p,name);
        Field agef = c.getDeclaredField("age");
        agef.setAccessible(true);
        agef.set(p,age);

        System.out.println(p.getName()); // "Xiao Ming"
        System.out.println(p.getAge()); // 20
    }

}

class Person {
    private String name;

    public Person(String name){

        this.name = name;
    }
    public String getName() {
        return this.name;
    }
}

class PersonTest {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}