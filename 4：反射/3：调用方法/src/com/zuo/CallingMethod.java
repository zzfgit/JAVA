package com.zuo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLOutput;

/*
* 调用方法
* 我们已经能通过Class实例获取所有Field对象，同样的，可以通过Class实例获取所有Method信息。Class类提供了以下几个方法来获取Method：

    Method getMethod(name, Class...)：获取某个public的Method（包括父类）
    Method getDeclaredMethod(name, Class...)：获取当前类的某个Method（不包括父类）
    Method[] getMethods()：获取所有public的Method（包括父类）
    Method[] getDeclaredMethods()：获取当前类的所有Method（不包括父类）
*
*
* */
public class CallingMethod {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        getMethod();
//        callMethod();
        callMethod2();
    }

    //1:获取方法
    static private void getMethod() throws NoSuchMethodException {
        Class stdClass = Student.class;
        //获取public方法getScore,参数为String
        System.out.println(stdClass.getMethod("getScore", String.class));
        //获取继承的public方法getName,无参数
        System.out.println(stdClass.getMethod("getName"));
        //获取private方法getGrade,参数为int
        System.out.println(stdClass.getDeclaredMethod("getGrade", int.class));
    }
    //2:调用方法
    //使用反射调用方法
    static private void callMethod() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //String对象
        String s = "Hello world";
        //获取String Substring(int)方法,参数为int:
        Method m = String.class.getMethod("substring", int.class);
        //在s对象上调用该方法病获取结果
        String sub = (String) m.invoke(s,6);
        //打印调用结果:
        System.out.println(sub);
    }

    //调用2个参数的方法
    static private void callMethod2() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //String对象
        String s = "Hello world";
        //获取String Substring(int)方法,参数为int:
        Method m = String.class.getMethod("substring", int.class, int.class);
        //在s对象上调用该方法病获取结果
        String sub = (String) m.invoke(s,6,8);
        //打印调用结果:
        System.out.println(sub);
    }



}

class Student extends Person {
    public int getScore(String type){
        return 99;
    }
    private int getGrade(int year) {
        return 1;
    }
}
class Person {
    public String getName() {
        return "Person";
    }
}