package com.getInherit;
/*
* 获取继承关系
*
*
    当我们获取到某个Class对象时，实际上就获取到了一个类的类型：

    Class cls = String.class; // 获取到String的Class
    还可以用实例的getClass()方法获取：

    String s = "";
    Class cls = s.getClass(); // s是String，因此获取到String的Class
    最后一种获取Class的方法是通过Class.forName("")，传入Class的完整类名获取：

    Class s = Class.forName("java.lang.String");
    这三种方式获取的Class实例都是同一个实例，因为JVM对每个加载的Class只创建一个Class实例来表示它的类型。
*
*
* */
public class GetInherit {
    public static void main(String[] args) throws ClassNotFoundException {
//        getClassInstance();
//        getSuperClassInstance();
//        getInterface();
        getSuperInterface();
    }


    //1:获取Class实例的三种方法
    private static void getClassInstance() throws ClassNotFoundException {
        Class cls1 = String.class;

        String s = "";
        Class cls2 = s.getClass();

        Class cls3 = Class.forName("java.lang.String");

        System.out.println(cls1);
        System.out.println(cls2);
        System.out.println(cls3);
    }

    //2:获取父类的Class
    //有了Class实例，我们还可以获取它的父类的Class：
    private static void getSuperClassInstance() {
        Class i = Integer.class;
        Class n = i.getSuperclass();
        System.out.println(n);//class java.lang.Number

        Class o = n.getSuperclass();
        System.out.println(o);//class java.lang.Object

        System.out.println(o.getSuperclass());//null
    }

    //3:获取interface
    //由于一个类可能实现一个或多个接口，通过Class我们就可以查询到实现的接口类型。例如，查询Integer实现的接口：
    //要特别注意：getInterfaces()只返回当前类直接实现的接口类型，并不包括其父类实现的接口类型：
    private static void getInterface(){
        Class s = Integer.class;
        Class[] is = s.getInterfaces();
        for (Class i:is){
            System.out.println(i);//interface java.lang.Comparable
        }
    }

    //4:获取父类实现的接口
    private static void getSuperInterface(){
        Class s = Integer.class.getSuperclass();
        Class[] is = s.getInterfaces();
        for (Class i:is){
            System.out.println(i);//interface java.io.Serializable
        }
    }
    //5:此外，对所有interface的Class调用getSuperclass()返回的是其父interface或者null：
    //因为接口类没有实现其他接口,所以获取不到父类实现的接口
    //如果一个类没有实现任何interface,那么getInterfaces()返回空数组

    //6:获取继承关系
    //当我们判断一个实例是否是某个类型时，正常情况下，使用instanceof操作符：
    //如果是两个Class实例，要判断一个向上转型是否成立，可以调用isAssignableFrom()：
    private static void getInherit() {

        //判断前边的是否是参数中的类的父类
        Integer.class.isAssignableFrom(Integer.class); //true，因为Integer可以赋值给Integer
        Number.class.isAssignableFrom(Integer.class); // true，因为Integer可以赋值给Number
        Object.class.isAssignableFrom(Integer.class); // true，因为Integer可以赋值给Object
        Integer.class.isAssignableFrom(Number.class); // false，因为Number不能赋值给Integer

    }
}
