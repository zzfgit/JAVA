package generic;

import org.junit.Test;

/*
* 自定义泛型类
*
* */
public class OrderTest {

    @Test
    public void test1(){

        //如果定义了反省类,实例化时没有指明泛型的类型,则认为此泛型类型为Object类
        Order<Object> order = new Order<>();
        order.setOrderT(123);
        order.setOrderT("123");

        //建议实例化时,指明泛型类型
        Order<String> order1 = new Order<>();
        order1.setOrderT("123");

        //这种就不行了,因为泛型被定义为了String类型,其他类型不可以
        //order1.setOrderT(123);

        SubOrder subOrder = new SubOrder();
        subOrder.setOrderT(123);

        //由于子类在集成带泛型的父类时,指明了泛型类型,则实例化子类对象时, 泛型类型就被确定了
        //subOrder.setOrderT("123");
    }
}
