package generic;

import org.junit.Test;
import java.util.*;

/*
* 泛型
*
* 1:JDK5.0中的新特性
*
* 2:在集合中使用泛型
*   总结:
*   1)集合接口和集合类在JDK5.0的时候都改为带泛型的结构
*   2)在实例化集合类是,可以指明具体的泛型类型
*   3)指定类型以后,在集合内部使用到类的泛型的位置,都定义为实例化传入的具体泛型类型
*   4)泛型的类型必须是类,不能是基本数据类型,需要用基本类型的位置,用包装类替换
*   5)如果实例化时,没有指明泛型的类型,默认是Object类型
*
* 3:自定义泛型类,泛型接口,泛型方法
*
*
* */
public class GenericTest {
    public static void main(String[] args) {

    }

    //在集合中使用泛型
    @Test
    public void test1(){
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(23);
        list.add(24);
        list.add(25);
        list.add(26);

        //编译时,进行类型检查,保证数据安全
//        list.add("string");

        for (Integer integer : list) {
            //省去了强转操作
            System.out.println(integer);
        }

        //迭代器也是带泛型的
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }

    @Test
    public void test2(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("tom",54);
        map.put("jerry",94);
        map.put("jack",84);
        map.put("john",24);

//        map.put(123,"错误的key类型");

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    public void test3(){
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();

        //不同泛型的对象不可以相互赋值
//        list2 = list1;
    }
}
