package generic;

/*
* 泛型类
* */
public class Order<T> {
    String orderName;
    int orderId;

    //在类内部使用类的泛型
    T orderT;

    public Order(){

    }

    public Order(String orderName, int orderId, T orderT) {
        this.orderName = orderName;
        this.orderId = orderId;
        this.orderT = orderT;
    }

    public T getOrderT(){
        return orderT;
    }
    public void setOrderT(T orderT){
        this.orderT = orderT;
    }

    //静态方法中不能使用泛型类型
    public static void show(){
//        System.out.println(orderT);
    }
}
