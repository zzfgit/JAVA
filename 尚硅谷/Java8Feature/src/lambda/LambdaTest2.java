package lambda;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/*
* 1:举例 : (o1,o2) -> Integer.compare(o1,o2);
* 2:格式:
*       ->:lambda操作符
*       ->左边:lambda形参列表
*       ->右边:lambda体(就是重写的抽象方法的方法体)
*
* 3:总结:
*   ->左边:lambda形参列表的参数类型可以省略(类型推断)
*   ->右边:lambda体应该使用{}包裹,如果lambda体只有一条语句,可以省略{} 和return
*
* 4:lambda表达式的本质:作为接口的实例
*
* */
public class LambdaTest2 {

    //语法格式一:无参无返回值
    @Test
    public void test1(){
        Runnable r2 = () -> {
            System.out.println("我爱故宫");
        };
        r2.run();
    }
    //语法格式二:有参无返回值
    @Test
    public void test2(){
        Consumer<String> con1 = (String s) -> {
            System.out.println(s);
        };
        con1.accept("有参无返回值的lambda表达式");
    }

    //语法格式三:数据;类型可以省略,因为可以有编译器推断得出,称为类型推断
    @Test
    public void test3(){
        Consumer<String> con1 = (String s) -> {
            System.out.println(s);
        };
        con1.accept("有参无返回值的lambda表达式");

        Consumer<String> con2 = (s) -> {
            System.out.println(s);
        };
        con2.accept("类型推断的lambda表达式");
    }

    //语法格式四:lambda若只需要一个参数,参数的小括号也可以省略
    @Test
    public void test4(){
        Consumer<String> con1 = (s) -> {
            System.out.println(s);
        };
        con1.accept("类型推断的lambda表达式");

        Consumer<String> con2 = s -> {
            System.out.println(s);
        };
        con2.accept("参数的小括号也可以省略的lambda表达式");
    }
    //语法格式五:lambda需要两个或以上的参数,多条执行语句,并有返回值
    @Test
    public void test5(){
        Comparator<Integer> com1 = new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return Integer.compare(integer,t1);
            }
        };
        int compare1 = com1.compare(12, 21);
        System.out.println(compare1);
        System.out.println("****************");

        Comparator<Integer> com2 = (o1,o2) -> {
            System.out.println(o1);
            System.out.println(o2);
            return o1.compareTo(o2);
        };
        System.out.println(com2.compare(12,3));
    }

    //语法格式六:当lambda体只有一条语句时,return与大括号都可以省略
    @Test
    public void test6(){


        Comparator<Integer> com1 = (o1,o2) -> {
            return o1.compareTo(o2);
        };
        System.out.println(com1.compare(12,3));
        System.out.println("****************");

        Comparator<Integer> com2 = (o1,o2) -> o1.compareTo(o2);
        System.out.println(com2.compare(12,3));
    }
}
