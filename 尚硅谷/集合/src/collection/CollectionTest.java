package collection;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

/*
* 一:集合的概述
* 1:集合,数组都是对多个数据进行存储,操作的结构,简称Java容器
*   此时的存储,值得是内存存储
* 2:数组的特点
*   1:一旦初始化,长度就确定了
*   2:数组一旦定义好,元素类型也就确定了
* 3:数组的缺点
*   1:长度无法更改
*   2:数组提供的方法很少,对于添加,删除等操作非常不方便,效率不高
*   3:获取数组中元素个数,数组没有提供属性或方法
*   4:数组存储数据的特点:有序,可重复.对于无序不可重复的需求,数组无法满足.
*
* 二:集合框架
*   Collection接口:单列集合,用来存储一个一个的数据
*       List接口:存储有序的,可重复的数据
*       Set接口:存储无序的,不可重复的数据
*   Map接口:双列集合,用来存储一对数据(key , value)
*
* */
public class CollectionTest {
    public static void main(String[] args) {

    }
    @Test
    public void test1(){
        Collection coll = new ArrayList();
        //1:add() 添加元素
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱
        coll.add(new Date());

        //2:size() 获取元素个数
        System.out.println(coll.size());//4

        //3:addAll() 将一个集合中的元素全部添加到另一个集合中
        Collection coll1 = new ArrayList();
        coll1.add(456);
        coll1.add("haha");
        coll1.addAll(coll);

        System.out.println(coll1);//[456, haha, aa, bb, 123, Sun Mar 01 22:09:03 CST 2020]

        //4:isEmpty 判断集合是否为空
        System.out.println(coll.isEmpty());//false

        //5:clear 清空集合中全部元素
//        coll.clear();
        System.out.println(coll);//[]

        //6:contaion() 是否包含某个值
        boolean contains = coll.contains(123);//false
        System.out.println(contains);//false

        //7:contaionAll() 判断形参中的元素是否都在集合中
        Collection coll2 = new ArrayList();
        coll2.add(123);
        coll2.add("aa");
        boolean contains1 = coll.containsAll(coll2);//true
        System.out.println(contains1);

        //8:remove() 移除元素
        coll.remove(123);
        System.out.println(coll);//[aa, bb, Sun Mar 01 22:34:02 CST 2020]

        //9:removeAll();从当前集合中移除指定集合中的所有相同元素(差集)
        coll.removeAll(coll2);
        System.out.println(coll);//[bb, Sun Mar 01 22:35:38 CST 2020]
    }
    @Test
    public void test2(){
        Collection coll = new ArrayList();
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱
        coll.add(new Date());
        //10:retainAll();获取当前集合和形参集合的交集,并修改到当前集合
        Collection coll1 = Arrays.asList(123,456,789);
        coll.retainAll(coll1);
        System.out.println(coll);

        //11:equals(Object obj);判断当前集合和形参是否相等
        Collection coll2 = new ArrayList();
        coll2.add("aa");
        coll2.add("bb");
        coll2.add(123);//自动装箱
        coll2.add(new Date());

        Collection coll3 = new ArrayList();
        coll3.add("aa");
        coll3.add("bb");
        coll3.add(123);//自动装箱
        coll3.add(new Date());

        boolean equals = coll3.equals(coll2);
        System.out.println(equals);//true

    }
    @Test
    public void test3(){

        Collection coll = new ArrayList();
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱
        coll.add(new Date());

        //12:hastCode()返回当前对象的哈希值
        System.out.println(coll.hashCode());//-1672721110

        //13:toArray();集合到数组的转换
        Object[] arr = coll.toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        //14:iterator();返回Iterator接口的实例,用于遍历集合元素
    }
}
