package collection.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/*
* Collection接口:单列集合,用来存储一个一个的数据
*    List接口:存储有序的,可重复的数据
*    Set接口:存储无序的,不可重复的数据
* Map接口:双列集合,用来存储一对数据(key , value)
*
* ArrayList,LinkedList,Vector三者的异同
* 同:三者都实现了list接口,存储数据的特点相同,存储有序,可重复的
*
* ArrayList:线程不安全的,效率高,底层使用Object[]存储
* LinkedList:底层使用双向链表存储,对于频繁插入删除操作效率比较高
* Vector:最初的list接口的实现类,线程安全的,效率低,底层使用Object[]存储
*
* 总结:常用的方法
* 增 add()
* 删 remove
* 改 set
* 查 get
* 插 add
* 长度 size()
* 遍历 1:Iterator迭代器
*     2:增强for循环
*     3:普通for循环
* */
public class ListTest {
    public static void main(String[] args) {

    }

    @Test
    public void test1(){
        ArrayList list = new ArrayList();

        //add()
        list.add(123);
        list.add(345);
        list.add("xiaowang");
        list.add(true);

        System.out.println(list);

        //add(index,obj)
        list.add(0,"插入第一位");
        System.out.println(list);

        //addAll();
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        list.addAll(list1);
        System.out.println(list);

        //get()
        System.out.println(list.get(2));//345

        //indexOf
        int index = list.indexOf("aaa");
        System.out.println(index);//-1

        //lastIndexOf
        int lastIndex = list.lastIndexOf(345);
        System.out.println(lastIndex);//2

        //remove 删除指定位置的元素
        list.remove(0);
        System.out.println(list);//[123, 345, xiaowang, true, 1, 2, 3]

        //set()
        list.set(0,"hahaha");
        System.out.println(list);//[hahaha, 345, xiaowang, true, 1, 2, 3]

        //subList
        List list2 = list.subList(2, 4);
        System.out.println(list2);//[xiaowang, true]
    }

    //遍历
    @Test
    public void test2(){
        ArrayList list = new ArrayList();
        list.add(123);
        list.add(345);
        list.add("xiaowang");
        list.add(true);

        //方式一:迭代器
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        //方式二:增强for循环
        for (Object o : list) {
            System.out.println(o);
        }

        //方式三:普通for循环
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
