package collection.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/*
* 增强for循环
* */
public class ForEachTest {
    public static void main(String[] args) {

    }

    @Test
    public void test(){
        Collection coll = new ArrayList();
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱
        coll.add(new Date());

        //foreach
        for (Object obj : coll) {
            System.out.println(obj);

        }
    }
}
