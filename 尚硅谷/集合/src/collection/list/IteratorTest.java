package collection.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/*
* 集合元素的遍历操作,使用Iterator接口
* 1:内部的方法hasNext() 和next()
* 2:集合对象内次调用iterator()方法都的到一个新的迭代器对象,默认的游标在集合的第一个元素之前
* 3:内部定一了remove(),可以在遍历的时候,删除指定元素,此方法不同于集合直接调用remove();
*
* */
public class IteratorTest {
    public static void main(String[] args) {

    }


    @Test
    public void test1(){
        Collection coll = new ArrayList();
        coll.add("aa");
        coll.add("bb");
        coll.add(123);//自动装箱
        coll.add(new Date());

        //方式一:
        Iterator iterator = coll.iterator();
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());

        //方式二:
        for (int i = 0; i < coll.size(); i++) {
            System.out.println(iterator.next());
        }

        //方式三:推荐使用的
        //hasNext判断是否还有下一个元素
        //next();1:指针下移 2:将指针下移后的元素返回
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
