package map;

import org.junit.Test;

import java.util.*;

/*
* Map的遍历
* */
public class MapTraverTest {
    public static void main(String[] args) {

    }

    @Test
    public void test1(){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("AA",11);
        map.put("DA",23);
        map.put("AD",43);
        map.put("GD",54);

        //遍历所有的key
        Set keyset = map.keySet();
        Iterator iterator = keyset.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        //遍历所有的value
        Collection<Object> values = map.values();
        for (Object value : values) {
            System.out.println(value);
        }

        //遍历所的key-value
        Set<Map.Entry<Object, Object>> entries = map.entrySet();
        Iterator<Map.Entry<Object, Object>> iterator1 = entries.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }
    }
}
