package map;

import org.junit.Test;

import java.util.HashMap;

/*
* Map中常用的方法
*
* */
public class MapMethod {
    public static void main(String[] args) {

    }
    @Test
    public void test(){
        HashMap<Object, Object> hashMap = new HashMap<>();

        //put
        hashMap.put("AA",123);
        hashMap.put(34,346);
        hashMap.put(45,5678);
        hashMap.put("BB",123);
        hashMap.put("AA",123);
        System.out.println(hashMap);

        //putAll
        HashMap<Object, Object> map = new HashMap<>();
        hashMap.put("FD",123);
        map.putAll(hashMap);
        System.out.println(map);

        //remove
        Object removeObject = map.remove(34);
        System.out.println(removeObject);//346
        System.out.println(map);//{AA=123, BB=123, 45=5678, FD=123}

        //clear 清空map里的数据
        hashMap.clear();
        System.out.println(hashMap);//{}
        System.out.println(hashMap.size());//0 长度变为0

        //get
        Object aa = map.get("AA");
        System.out.println(aa);//123

        //containsKey
        boolean isExist = map.containsKey("BB");
        System.out.println(isExist);//true

        boolean asssIsExist = map.containsValue("asss");
        System.out.println(asssIsExist);//false

        //isEmpty
        boolean empty = map.isEmpty();
        System.out.println(empty);//false
    }
}
