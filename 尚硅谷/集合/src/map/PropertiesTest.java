package map;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/*
* 主要用来处理配置文件
* */
public class PropertiesTest {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        FileInputStream fis = new FileInputStream("jdbc.properties");
        properties.load(fis);

        String name = properties.getProperty("name");
        System.out.println(name);
        String pwd = properties.getProperty("pwd");
        System.out.println(pwd);

        fis.close();
    }
}
