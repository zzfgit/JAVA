import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
* collections:是操作collection和Map的工具类
*
* //面试题
* collection和collections的区别:
* 一个是接口,一个是工具类
* */
public class CollectionsTest {
    public static void main(String[] args) {

    }

    @Test
    public void test1(){

        List list = new ArrayList();
        list.add(23);
        list.add(43);
        list.add(54);
        list.add(54);
        list.add(54);
        list.add(234);
        list.add(76);

        //reverse(List)//反转
        Collections.reverse(list);
        System.out.println(list);//[76, 234, 54, 43, 23]

        //随机打乱顺序
        Collections.shuffle(list);
        System.out.println(list);//[43, 234, 54, 23, 76]

        //sort//培训
        Collections.sort(list);
        System.out.println(list);//[23, 43, 54, 76, 234]

        //frequency 统计某个值出现的次数
        int frequency = Collections.frequency(list,54);
        System.out.println(frequency);//3

        //copy//赋值数组中的元素到另一个数组
        List<Object> destList = Arrays.asList(new Object[list.size()]);
        Collections.copy(destList,list);
        System.out.println(destList);//[23, 43, 54, 54, 54, 76, 234]
    }

    /*
    * Collections类提供了多个synchronizedXXX()方法,该方法可将制定集合包装成线程同步的集合,从而解决多线程并发访问时的线程安全问题
    * */
    @Test
    public void test2(){

        List list = new ArrayList();
        list.add(23);
        list.add(43);
        list.add(54);
        list.add(54);
        list.add(54);
        list.add(234);
        list.add(76);

        List list1 = Collections.synchronizedList(list);
        System.out.println(list1);//[23, 43, 54, 54, 54, 234, 76]
    }
}
