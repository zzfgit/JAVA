package reflect;

import javafx.animation.Animation;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class MethodTest {

    public static void main(String[] args) {

    }


    @Test
    public void test1(){
        Class clazz = Person.class;

        //获取当前类和父类的所有public的方法
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println();

        //获取当前类的所有方法(不包括父类的方法)
        Method[] methods1 = clazz.getDeclaredMethods();
        for (Method method : methods1) {
            System.out.println(method);
        }
    }

    /*
    * 获取方法的结构
    * 修饰符 返回值类型 方法名
    * */
    @Test
    public void test2(){
        Class clazz = Person.class;

        //获取当前类的所有方法(不包括父类的方法)
        Method[] methods1 = clazz.getDeclaredMethods();
        for (Method method : methods1) {
            System.out.println("方法:" + method);
            //1 获取方法的注解
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation : annotations) {
                System.out.println(annotation);
            }

            //2 权限修饰符
            System.out.println("修饰符:" + method.getModifiers());

            //3 返回值类型
            System.out.println("返回值类型:" + method.getReturnType());

            //4 方法名
            System.out.println("方法名:" + method.getName());

            //5 参数列表
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (!(parameterTypes == null && parameterTypes.length == 0)) {
                for (Class<?> parameterType : parameterTypes) {
                    System.out.println("参数类型:" + parameterType);
                }
            }

            //6 抛出的异常
            Class<?>[] exceptionTypes = method.getExceptionTypes();
            for (Class<?> exceptionType : exceptionTypes) {
                System.out.println("异常类型:" + exceptionType);
            }
            System.out.println();
        }
    }
}
