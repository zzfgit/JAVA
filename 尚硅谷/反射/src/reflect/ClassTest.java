package reflect;

import org.junit.Test;

/*
* Class类的理解
* 1:类的加载过程:
*   程序经过javac.exe命令后,会生成一个或多个字节码文件(.class结尾),
*   然后使用java.exe命令对某一个字节码文件进行解释运行,相当于将某一个字节码文件加载到内存中,此过程就称为类的加载过程.加载到内存中的类,就称为运行时类,就作为Class类的一个实例
*
*
* */
public class ClassTest {

    /*
     * 获取Class实例的方式
     *
     * 前三种方式需要掌握
     * */
    @Test
    public void test1() throws ClassNotFoundException {

        //方式一:调用运行时类的属性 .class
        Class clazz1 = Person.class;
        System.out.println(clazz1);

        //方式二:通过运行时类的对象 .getClass()
        Person p1 = new Person();
        Class<? extends Person> clazz2 = p1.getClass();
        System.out.println(clazz2);

        //方式三:调用Class的静态方法:forName(String classPath)
        Class<?> clazz3 = Class.forName("reflect.Person");
        System.out.println(clazz3);

        //获取到的Class对象都是同一个
        System.out.println(clazz1 == clazz2);//true
        System.out.println(clazz3 == clazz2);//true

        //方式四:使用类的加载器:ClassLoader(了解)
//        ClassLoader classLoader = ReflectionTest.class.getClassLoader();
//        Class clazz4 = classLoader.loadClass("class reflect.Person");
//        System.out.println(clazz4);

    }
}