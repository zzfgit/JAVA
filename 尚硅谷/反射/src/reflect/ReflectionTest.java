package reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
* 反射
* */
public class ReflectionTest {

    //反射之前,对于Person的操作
    @Test
    public void test1(){
        //1:创建Person类的对象
        Person xiaoli = new Person();
        //2:通过对象,调用方法
        xiaoli.show();

        //在Person类的外部,不可以通过Person对象调用私有方法和私有属性
    }

    //反射之后,对于Person的操作
    @Test
    public void test2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Class clazz = Person.class;

        //通过反射,创建Person类的对象
        Constructor cons = clazz.getConstructor(String.class, int.class);

        Object xiaoming = cons.newInstance("xiaoming", 23);
        System.out.println(xiaoming);

        //2:通过反射,调用对象指定的属性方法
        Field age = clazz.getDeclaredField("age");
        age.set(xiaoming,12);

        System.out.println(xiaoming);

        //调用方法
        Method show = clazz.getDeclaredMethod("show");
        show.invoke(xiaoming);//我是一个人

        //通过反射可以调用类的私有结构,比如类的私有构造器,私有方法,私有属性

        //调用私有属性
        Field name = clazz.getDeclaredField("name");
        name.setAccessible(true);
        name.set(xiaoming,"小明");
        System.out.println(xiaoming);
    }


}
