package reflect;

import org.junit.Test;

import java.util.Properties;

public class ClassLoaderTest {

    @Test
    public void test1(){
        //对于自定义类,使用系统类加载器进行加载
        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
        System.out.println(classLoader);

        //系统类加载器的getParent():获取扩展类加载器
        ClassLoader classLoader1 = classLoader.getParent();
        System.out.println(classLoader1);

        //调用扩展类加载器的getParent():无法获取到引导类加载器
        ClassLoader classLoader2 = classLoader1.getParent();
        System.out.println(classLoader2);//null
        //引导类加载器主要负责加载Java的核心类库,无法加载自定义类
    }

    /*
    * Properties:用来读取配置文件
    *
    * */
    @Test
    public void test2(){
        Properties pros = new Properties();
    }

}
