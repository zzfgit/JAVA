package reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;

public class OtherTest {
    /*
    * 获取构造器结构
    * */
    @Test
    public void test1() throws NoSuchMethodException {
        Class<Person> personClass = Person.class;

        //获取当前运行时类中的public的构造器
        Constructor<?>[] constructors = personClass.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println("构造方法:" + constructor);
        }

        //获取当前运行时类中的所有的构造器
        Constructor<?>[] constructors1 = personClass.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors1) {
            System.out.println("构造方法:" + constructor);
        }
    }

    @Test
    public void test2(){
        Class<Person> personClass = Person.class;

        //获取运行时类的父类
        Class<? super Person> superclass = personClass.getSuperclass();
        System.out.println(superclass);
    }
}
