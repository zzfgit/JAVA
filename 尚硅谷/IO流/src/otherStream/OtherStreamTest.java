package otherStream;

import org.junit.Test;

import java.io.*;

/*
* 其他流的使用
* 1:标准的输入,输出流
* 2:打印流
* 3:数据流
*
* */
public class OtherStreamTest {

    /*
    * System.in 输入流
    * System.out 输出流
    *
    * 练习:从键盘输入字符串,将读取到的整行的字符串转换成大写输出,然后继续进行输入操作,直至输入"e"或者"exit"时,退出程序
    *   方法一:使用Scanner实现
    *   方法二:使用System.in实现
    * */
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bfr = new BufferedReader(isr);

        while (true) {
            System.out.println("请输入字符串:");
            String data = bfr.readLine();
            if ("e".equalsIgnoreCase(data) || "exit".equalsIgnoreCase(data)){
                System.out.println("程序结束");
                break;
            }
            String upperCase = data.toUpperCase();
            System.out.println(upperCase);
        }
        if (bfr != null)
            bfr.close();
    }

    /*
    * 数据流:
    * DataOutputStream:数据输出流
    * 用于写入对象
    * */
    @Test
    public void test3(){
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("data.txt"));
//            dataOutputStream.write(111);
//            dataOutputStream.writeUTF("aaa");
            dataOutputStream.writeUTF("中国五星红旗");

            dataOutputStream.writeBoolean(true);

            dataOutputStream.flush();//刷新操作,将内存中的数据写入到内存
            dataOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    /*
    * 数据输入流
    * DataInputStream:将磁盘中间中的数据输入到程序中,保存成变量
    *
    * 注意:读取的顺序,要与写入的数据顺序对应
    * */
    @Test
    public void test4() throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("data.txt"));
        String i = dataInputStream.readUTF();
        System.out.println(i);
        dataInputStream.close();
    }

}
