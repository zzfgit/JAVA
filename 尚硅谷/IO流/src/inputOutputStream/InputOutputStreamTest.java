package inputOutputStream;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/*
* 测试FileInputStream和FileOutputStream
*
* 结论:
* 1:对于文本文件(.txt,.java,.c,.cpp),使用字符流来处理
* 2:对于非文本文件(.jpg,.mp3,.mp4,.avi,.doc,.ppt),使用字节流来处理
*
* */
public class InputOutputStreamTest {

    /*
    * 使用字节流处理文本文件,可能会出现乱码,如果有中文或者其他文本
    * */
    @Test
    public void testFileInputStream(){
        FileInputStream fileInputStream = null;
        try {
            //1:文件对象
            File file = new File("hello.txt");

            //2:流对象
            fileInputStream = new FileInputStream(file);

            //3:读数据
            byte[] buffer = new byte[5];
            int len;
            while ((len = fileInputStream.read(buffer)) != -1){
                String s = new String(buffer,0,len);
                System.out.print(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4:关闭流资源
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
    * 实现对图片的复制
    * */
    @Test
    public void testFileInputOutputStream(){
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            File srcFile = new File("image.jpeg");
            File destFile = new File("image1.jpeg");

            fileInputStream = new FileInputStream(srcFile);
            fileOutputStream = new FileOutputStream(destFile);

            //复制的过程
            byte[] bytes = new byte[5];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1){
                fileOutputStream.write(bytes,0,len);
            }
            System.out.println("复制成功");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //指定路径下文件的赋值

    public void copyFile(String srcPath,String destPath){
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            File srcFile = new File(srcPath);
            File destFile = new File(destPath);

            fileInputStream = new FileInputStream(srcFile);
            fileOutputStream = new FileOutputStream(destFile);

            //复制的过程
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1){
                fileOutputStream.write(bytes,0,len);
            }
            System.out.println("复制成功");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void testCopyFile(){
        long start = System.currentTimeMillis();

        String srcPath = "/Users/zuozhongfei/Music/周杰伦/依然范特西/周杰伦,费玉清 - 千里之外.mp3";
        String destPath = "/Users/zuozhongfei/Music/周杰伦/依然范特西/周杰伦,费玉清 - 千里之外2.mp3";

        copyFile(srcPath,destPath);

        long end = System.currentTimeMillis();

        System.out.println(end-start);
    }
}
