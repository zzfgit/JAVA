package buffer;

import org.junit.Test;

import java.io.*;

/*
* 缓冲流
*
* 作用:提高读取和写入的速度
*
* */
public class BufferTest {

    /*
    * 实现非文本文件的复制
    * */
    @Test
    public void bufferStreamTest(){
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutpytStream = null;
        try {
            //1:造文件
            File srcFile = new File("image.jpeg");
            File destFile = new File("image3.jpeg");

            //2:造流对象
            //2.1:造节点流
            fileInputStream = new FileInputStream(srcFile);
            fileOutputStream = new FileOutputStream(destFile);
            //2.2:造缓冲流
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            bufferedOutpytStream = new BufferedOutputStream(fileOutputStream);


            //3:复制数据
            byte[] bytes = new byte[1024];
            int len;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                bufferedOutpytStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        //4:关闭流资源
        //先关闭外层的流,在关闭内层的流
        try {
            if (bufferedOutpytStream != null)
                bufferedOutpytStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (bufferedInputStream != null)
                bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //关闭外层流的时候,会自动关闭内层流
//        try {
//            fileOutputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fileInputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    /*
    * 使用缓冲流实现复制文件
    *
    * */
    public void copyFileWithBufferd(String srcPath,String destPath){
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutpytStream = null;
        try {
            //1:造文件
            File srcFile = new File(srcPath);
            File destFile = new File(destPath);

            //2:造流对象
            //2.1:造节点流
            fileInputStream = new FileInputStream(srcFile);
            fileOutputStream = new FileOutputStream(destFile);
            //2.2:造缓冲流
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            bufferedOutpytStream = new BufferedOutputStream(fileOutputStream);


            //3:复制数据
            byte[] bytes = new byte[1024];
            int len;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                bufferedOutpytStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        //4:关闭流资源
        //先关闭外层的流,在关闭内层的流
        try {
            if (bufferedOutpytStream != null)
                bufferedOutpytStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (bufferedInputStream != null)
                bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /*
    * 测试结论:缓冲流复制文件比普通读写流复制文件快很多
    *   因为缓冲流内部有一个缓冲区
    *
    * */
    @Test
    public void testCopyFileWithBufferd(){
        long start = System.currentTimeMillis();

        String srcPath = "/Users/zuozhongfei/Music/周杰伦/依然范特西/周杰伦,费玉清 - 千里之外.mp3";
        String destPath = "/Users/zuozhongfei/Music/周杰伦/依然范特西/周杰伦,费玉清 - 千里之外2.mp3";

        copyFileWithBufferd(srcPath,destPath);

        long end = System.currentTimeMillis();
        System.out.println(end-start);
    }

    /*
    * 使用BufferedReader和BufferedWriter实现文本文件的赋值
    * */

    @Test
    public void testBufferedReadBufferedWrite(){

        //创建文件和对应的流
        BufferedWriter bw = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File("hello.txt")));
            bw = new BufferedWriter(new FileWriter(new File("hello4.txt")));

            //读写操作
            //方式一:使用char[]
//            char[] chars = new char[1024];
//            int len;
//            while ((len = br.read(chars)) != -1){
//                bw.write(chars,0,len);
//    //            bw.flush();//刷新缓冲区,如果缓冲区有数据,立刻进行写入操作
//            }

            //方式二:使用String
            String data;
            while ((data = br.readLine()) != null) {
                //手动插入换行
                //方法一:
//                bw.write(data + "\n");//data中不包含换行符

                //方法二
                bw.write(data );//data中不包含换行符
                bw.newLine();//写入新的一行
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
