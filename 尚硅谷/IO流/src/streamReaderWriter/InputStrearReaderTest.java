package streamReaderWriter;

import org.junit.Test;

import java.io.*;

/*
* 一:转换流:属于字符流
* 1:InputStreamReader:将一个字节的输入流转换为一个字符输入流
* 2:OutputStreamWriter:将一个字符的输出流转换为一个字节的输出流
*
* 二:作用
*   提供字节流与字符流之间的转换
*
* 三:
*   解码:字节,字节数组 --> 字符串,字符数组
*   编码:字符数组,字符串 --> 字节,字节数组
*
* 四:字符集
*   ASCII:美国标准的信息交换码.
*       用一个字节的7位可以表示
*   ISO8859-1:拉丁码表,欧洲码表
*       用一个字节的8位表示
*   GB2312:中文的编码表,最多2个字节
*   GBK:中国的中文编码升级,加入了更多的中文文字符号,最多两个字节编码
*   Unicode:国际标准码,融合了目前人类使用的所有字符,每个字符分配唯一的字符码,所有的文字都用两个字节表示
*   UTF-8:变长的编码方式,可用1-4个字节表示一个字符,指定了一个字符码写入到磁盘中的具体的编码方式
*
* */
public class InputStrearReaderTest {

    /*
    * 字符输入转换流
    * */
    @Test
    public void inputStreamReaderTest() throws IOException {
        FileInputStream fis = new FileInputStream("hello.txt");
        InputStreamReader isr = new InputStreamReader(fis,"utf-8");

        char[] chars = new char[16];
        int len;
        while ((len = isr.read(chars)) != -1){
            String str = new String(chars,0,len);
            System.out.println(str);
        }
        isr.close();
    }

    /*
    * 综合使用InputStreamReader和OutputStreamWriter
    *
    * */
    @Test
    public void test2() throws IOException {
        FileInputStream fis = new FileInputStream("hello.txt");
        FileOutputStream fos = new FileOutputStream("hello5.txt");


        InputStreamReader isr = new InputStreamReader(fis,"utf-8");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"gbk");

        char[] chars = new char[16];
        int len;
        while ((len = isr.read(chars)) != -1){
            String str = new String(chars,0,len);
            System.out.println(str);

            osw.write(chars,0,len);

        }
        isr.close();
        osw.close();
    }
}
