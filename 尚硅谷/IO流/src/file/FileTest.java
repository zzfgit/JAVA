package file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/*
* 1:File类的一个对象,代表一个文件或者一个文件目录
*
* */
public class FileTest {
    /*
     * 创建一个File类实例
     * */
    @Test
    public void test1() {

        //相对路径:相对于当前的module文件夹内
        File file1 = new File("hello.txt");

        //绝对路径:包含盘符在内的文件或文件目录的路径

        File file2 = new File("/Users/zuozhongfei/Desktop/学习/JAVA/尚硅谷/IO流/hi.txt");

        //使用一个对象类初始化另一个对象
        File file4 = new File(file1, "aaa.txt");
        System.out.println(file4);
    }

    /*
    * File类的获取功能
    * */
    @Test
    public void test2(){
        File file1 = new File("hello.txt");

        System.out.println(file1.getAbsoluteFile());
        System.out.println(file1.getName());
        System.out.println(file1.getParent());
        System.out.println(file1.length());
        System.out.println(file1.lastModified());

        File file2 = new File("/Users/zuozhongfei/Desktop/学习/JAVA/尚硅谷/IO流");

        //获取文件夹下所有文件名的字符串数组
        String[] listString = file2.list();

        //获取文件夹下的所有文件的数组
        File[] filesListString = file2.listFiles();

        for (String s : listString) {
            System.out.println(s);
        }

        for (File file : filesListString) {
            System.out.println(file);
        }
    }

    @Test
    public void test3() {
        File file1 = new File("hello.txt");
        File file2 = new File("/Users/zuozhongfei/Desktop/学习/JAVA/尚硅谷/IO流/hi.txt");

        //要想改名成功,需要保证要赋值的File对象是存在的
        boolean b = file1.renameTo(file2);
        System.out.println(b);
    }

    //创建硬盘中对应的文件
    @Test
    public void test4() throws IOException {
        File file1 = new File("hahaha");
        if (!file1.exists()){
            file1.createNewFile();
            System.out.println("创建成功");
        }else{
            file1.delete();
            System.out.println("删除成功");
        }
    }

    //创建硬盘中对应的文件目录
    //如果创建文件时,文件已经存在就返回创建失败
    //如果创建多层文件夹时,上层文件夹也不存在,会一并创建
    @Test
    public void test5() throws IOException {
        File file1 = new File("/Users/zuozhongfei/Desktop/学习/JAVA/尚硅谷/IO流/IO1");
        boolean mkdir = file1.mkdir();
        if (mkdir) {
            System.out.println("创建成功");
        }else{
            System.out.println("创建失败");
        }

        File file2 = new File("/Users/zuozhongfei/Desktop/学习/JAVA/尚硅谷/IO流/IO2/IO3");
        boolean mkdir1 = file2.mkdirs();
        if (mkdir1) {
            System.out.println("创建多层文件夹成功");
        }else{
            System.out.println("创建多层文件夹失败");
        }
    }
}
