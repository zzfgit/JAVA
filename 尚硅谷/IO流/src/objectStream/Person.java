package objectStream;

import java.io.Serializable;

public class Person implements Serializable {

    //对象要支持系列化和反序列化不许实现 Serializable 接口
    //并且定义一个常量SerialVersionUID

    //其内部的所有属性也必须实现 Serializable 接口

    private static final long SerialVersionUID = 21345223452L;

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
