import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
* 创建线程的方式四:线程池
*
* 1:提高响应速度(较少了创建线程的事件)
* 2:降低资源消耗(不会频繁创建销毁线程)
* 3:便于线程的管理
*       设置线程池的大小
*       最大线程数
*       线程空闲最长事件后终止
* */
public class ThreadPool {
    public static void main(String[] args) {

//       1: 创建一个线程池,最大线程数是10
        ExecutorService service = Executors.newFixedThreadPool(10);

//        2:执行指定的线程的操作,需要提供实现Runable接口或者Callable接口的实现类对象
        service.execute(new NumberThread());//适用于Runable
//        service.submit();//适用于Callable
//      3:关闭连接
        service.shutdown();
    }
}

class NumberThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i%2 == 0){
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }
        }
    }
}

