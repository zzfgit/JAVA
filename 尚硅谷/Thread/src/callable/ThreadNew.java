package callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/*
* 创建线程的方式三:实现callable接口 JDK5.0更新
*
* 1:创建一个实现Callable接口的类
* 2:实现call方法,将此线程需要执行的操作写到call方法中
* 3:创建callable接口实现的类对象
* 4:将Callable接口实现类的对象作为参数传递到FutureTask的构造其中,创建FutureTask对象
* 5:将FutureTask类对象作为参数传递到Thread类的构造方法中,创建Thread对象,调用start();
*
* 如何理解实现Callable接口的方式比实现Runable方式要强大
* 1:call() 方法有返回值
* 2:call() 方法可以抛出异常
* 3:Callable是支持泛型的
* */
public class ThreadNew {
    public static void main(String[] args) {
        ThreadCall c1 = new ThreadCall();

        FutureTask ftask = new FutureTask(c1);

        new Thread(ftask).start();

        try {
            //get方法的返回值,就是FutureTask的线程对象的call方法的返回值
            Object sum = ftask.get();
            System.out.println("总和为:" + sum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class ThreadCall implements Callable {

    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
                sum += i;
            }
        }
        return sum;
    }
}