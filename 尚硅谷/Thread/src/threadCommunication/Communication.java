package threadCommunication;

/*
* 线程通信:
* 三个方法:
*   1:wait();当前线程进入阻塞状态,并释放同步监视器
*   2:notify();唤醒被wait的一个线程,如果有多个线程被wait,就唤醒优先级高的
*   3:notifyAll();唤醒所有的被wait的线程
*
*   注意:这三个方法,只能在同步方法或者同步代码块里
*       这三个方法的调用者,必须是同步代码快或者同步方法的同步监视器对象
*       这三个方法是定义的Object类中
*
* 面试题:sleep方法和wait方法的异同
*       同:阻塞当前线程
*       不同:1:两个方法声明位置不一样,sleep()声明在Thread类,wait()声明在Object中
*           2:调用的范围不一样,sleep()可以在任何需要的地方调用,wiat()必须在同步代码块和同步方法中
*           3:关于是否释放同步监视器:如果两个都在同步代码块或同步方法中,sleep不会释放锁,wait会释放锁.
* */
class Number implements Runnable{
    private int number = 1;

    @Override
    public void run() {
        while (true) {
            synchronized (this){

                notify();//唤醒正在阻塞状态的线程
//                notifyAll();

                if (number <= 100){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + " : " + number);
                    number ++;
                    try {
                        wait();//调用wait();当前线程进入阻塞
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    break;
                }
            }
        }
    }
}

public class Communication {
    public static void main(String[] args) {
        Number n1 = new Number();

        Thread t1 = new Thread(n1);
        Thread t2 = new Thread(n1);

        t1.start();
        t2.start();
    }
}
