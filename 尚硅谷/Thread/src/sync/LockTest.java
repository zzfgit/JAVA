package sync;

import java.util.concurrent.locks.ReentrantLock;

/*
* 解决线程安全的问题三:Lock锁
*
* 面试题1:synchronized和lock方式的异同
* 同:都可以解决线程安全问题
* 异:synchronized执行完代码逻辑之后,可以自动释放锁
*   lock需要手动的加锁,解锁,但是更灵活
*
* 线程同步的使用顺序
* lock > 同步代码块 > 同步方法
*
* 面试题2:如何解决线程安全问题?有几种
* lock > 同步代码块 > 同步方法
* */
public class LockTest {
    public static void main(String[] args) {
        Window1 w = new Window1();
        Thread t1 = new Thread(w);
        Thread t2 = new Thread(w);
        Thread t3 = new Thread(w);

        t1.start();
        t2.start();
        t3.start();
    }
}

class Window1 implements Runnable{
    private int ticket = 100;

    //1:实例化一个lock
    private ReentrantLock lock = new ReentrantLock(true);
    @Override
    public void run() {
        while (true){
            try {
                //2:调用lock,加锁
                lock.lock();
                if (ticket > 0) {
                    System.out.println(Thread.currentThread().getName() + " 买票:票号为: " + ticket);
                    ticket --;
                }else{
                    break;
                }
            }finally {
                //3:调用unlock,解锁
                lock.unlock();
            }

        }
    }
}