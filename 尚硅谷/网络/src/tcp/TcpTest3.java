package tcp;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/*
* 从客户端发送文件给服务端,服务端保存到本地,并返回保存成功给客户端
* */
public class TcpTest3 {

    @Test
    public void client() throws IOException {

        //1:socket
        Socket socket = new Socket(InetAddress.getByName("127.0.0.1"),9999);
        //2:输出流
        OutputStream os = socket.getOutputStream();

        //3:文件流
        FileInputStream fis = new FileInputStream(new File("image.jpeg"));
        //4:写入文件(输出文件)
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1){
            os.write(buffer,0,len);
        }

        //关闭数据输出
        socket.shutdownOutput();

        //5:接收服务器端返回的数据
        InputStream is = socket.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] bytes = new byte[20];
        int len1;
        while ((len1 = is.read(bytes)) != -1){
            baos.write(bytes,0,len1);
        }
        System.out.println(baos.toString());

        //6:资源关闭
        fis.close();
        os.close();
        socket.close();
        is.close();
        baos.close();
    }

    @Test
    public void server(){
        ServerSocket ss = null;
        Socket socket = null;
        InputStream is = null;
        FileOutputStream fos = null;
        OutputStream os = null;
        try {
            ss = new ServerSocket(9999);

            socket = ss.accept();

            is = socket.getInputStream();

            fos = new FileOutputStream(new File("image3.jpeg"));

            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                fos.write(buffer,0,len);
            }



            //服务器端给与客户端反馈
            os = socket.getOutputStream();
            os.write("图片已收到!".getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
