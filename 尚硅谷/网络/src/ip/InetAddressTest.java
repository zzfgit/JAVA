package ip;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*
* InetAddress:表示IP地址的类
*
*
* */
public class InetAddressTest {

    public static void main(String[] args) {
        try {
            InetAddress ip = InetAddress.getByName("192.168.0.12");
            System.out.println(ip);

            InetAddress ip2 = InetAddress.getByName("www.baidu.com");
            System.out.println(ip2);

            InetAddress ip3 = InetAddress.getByName("localhost");
            System.out.println(ip3);

            //获取本机IP地址
            InetAddress ip4 = InetAddress.getLocalHost();
            System.out.println(ip4);

            //两个常用方法
            //获取主机名
            System.out.println(ip2.getAddress());
            //获取IP地址
            System.out.println(ip2.getHostName());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
