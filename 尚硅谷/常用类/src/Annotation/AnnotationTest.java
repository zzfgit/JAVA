package Annotation;

/*
* 注解
* 1:jdk5.0新增的功能
* 2:是代码里的特殊标记
* 3:元注解,对现有注解的注解
*   //比较常用的元注解
*   retention:指定所修饰的annotation的生命周期
*   target:指定注解的作用的范围
*
*   //用的比较少的注解
*   documented:标识所修饰的注解被Javadoc解析时,保留下来
*   inherited:表示注解具有继承性,修饰父类的注解会被子类继承
*
* 4:JDK8中注解的新特性:
*   1)可重复注解
*   2)类型注解
* */
public class AnnotationTest {

    public static void main(String[] args) {

    }

    @MyAnnotation("hi")
    public void test(){

    }
}
