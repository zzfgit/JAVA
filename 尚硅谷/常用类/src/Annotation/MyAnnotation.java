package Annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
* 自定义注解
* 1:注解声明为@intereface
* 2:内部成员变量,通常使用value标识
* 3:可以指定默认的成员变量,使用default
* 4:如果自定义注解没有成员变量,表明是一个标识
* 5:如果注解有成员变量,需要指定值
*
* 自定义注解必须配上注解的信息处理流程(使用反射)才有意义
* */
@Retention(RetentionPolicy.RUNTIME)//注解的生命周期
@Target(ElementType.METHOD)//指定注解的作用的范围
public @interface MyAnnotation {

    String value() default "hello";

}
