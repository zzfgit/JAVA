import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

public class CompareTest {
    /*
    * Java中的对象,默认情况下,只能进行 == != 比较,不能比较大小
    * 但是开发中,我们需要对多个对象进行排序,就需要对对象进行比较大小
    *
    * 实现:我们需要使用
    *   1:Comparable (自然排序)
    *   2:Comparator (定制排序)
    *
    * 使用:
    *   1:Comparable的使用
    *       自定义类,如果需要排序,需要实现comparable接口
    *
    *   2:Comparator(定制排序)
    *       当自定义元素的类型没有实现comparable接口而又不方便修改代码,
    *       那么就需要使用Comparator来排序
    *
    * 比较:
    *   Comparable需要比较的类类实现比较方法
    *   Comparator,保证需要把比较的对象在任何时候都可以比较大小
    *
    * */
    public static void main(String[] args) {

    }

    @Test
    public void test1(){
        //String默认可以排序,因为String实现了Comparable接口
        String[] arr = new String[]{"aa","dd","ii","ww"};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));//[aa, dd, ii, ww]
    }

    @Test
    public void test2(){
        Goods[] arr = new Goods[4];
        arr[0] = new Goods("小米",29);
        arr[2] = new Goods("戴尔",199);
        arr[3] = new Goods("苹果",599);
        arr[1] = new Goods("联想",87);

        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));//[name :小米 price :29.0, name :联想 price :87.0, name :戴尔 price :199.0, name :苹果 price :599.0]
    }

    @Test
    public void test3(){
        String[] arr = new String[]{"aa","dd","ii","ww"};
        Arrays.sort(arr, new Comparator<String>() {

            //指定定制排序规则,按照字符创从大到小排序
            @Override
            public int compare(String s, String t1) {
                return -s.compareTo(t1);
            }
        });
        System.out.println(Arrays.toString(arr));//[ww, ii, dd, aa]
    }

    @Test
    public void test4(){
        Goods[] arr = new Goods[4];
        arr[0] = new Goods("小米",29);
        arr[2] = new Goods("戴尔",199);
        arr[3] = new Goods("苹果",599);
        arr[1] = new Goods("联想",87);

        Arrays.sort(arr, new Comparator<Goods>() {
            //定制排序规则,先按照产品名称从低到高,然后按照价格从高到低
            @Override
            public int compare(Goods goods, Goods t1) {
                if (goods.getName().equals(t1.getName())){
                    return -Double.compare(goods.getPrice(),t1.getPrice());
                }else{
                    return goods.getName().compareTo(t1.getName());
                }
            }
        });
        System.out.println(Arrays.toString(arr));//[name :小米 price :29.0, name :联想 price :87.0, name :戴尔 price :199.0, name :苹果 price :599.0]
    }
}

class Goods implements Comparable{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private String name;
    private double price;

    public Goods(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return new String("name :" + this.name + " price :" + this.price);
    }

    //按照商品价格排序
    @Override
    public int compareTo(Object o) {
        if (o instanceof Goods){
            Goods goods = (Goods)o;
            if (this.price>goods.price){
                return 1;
            }else if(this.price<goods.price){
                return -1;
            }else{
                return 0;
            }
        }
        throw new RuntimeException("传入的数据类型不一致");
    }
}