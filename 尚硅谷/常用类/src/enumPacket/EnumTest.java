package enumPacket;

import org.junit.Test;

/*
 * 枚举的使用
 *
 * 定义枚举类
 * 1:在JDK5.0之前,自定义枚举类
 * 2:在JDK5,0之后,使用enum关键字定义枚举类
 *
 * */
public class EnumTest {
    public static void main(String[] args) {

    }

    @Test
    public void test() {
        System.out.println(Season.SUMMER);
    }
}

//自定义枚举类
class Season {

    //1:声明Season对象的属性
    private final String seasonName;
    private final String seasonDesc;

    //2:私有化类的构造器,并且给对象属性赋值
    private Season(String seasonName, String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    //3:提供当前枚举类的多个对象
    public static final Season SPRING = new Season("春天","春暖花开");
    public static final Season SUMMER = new Season("夏天","夏日炎炎");
    public static final Season AUTUMN = new Season("球天","秋高气爽");
    public static final Season WINTER = new Season("冬天","冰天雪地");

    //4:获取枚举类对象的属性
    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }

    //5:提供toString方法

    @Override
    public String toString() {
        return new String(this.seasonName + " : " + this.seasonDesc);
    }
}