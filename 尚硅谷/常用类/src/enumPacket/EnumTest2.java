package enumPacket;
/*
* 使用enum关键字定义枚举类
* */

public class EnumTest2 {

    public static void main(String[] args) {
        Season1 summer = Season1.SUMMER;
        System.out.println(summer);

        Gender man = Gender.MAN;
        System.out.println(man);
    }

}

enum Season1 {

    //定义四个枚举值,每个值之间用逗号隔开
    SPRING("春天","春暖花开"),
    SUMMER("夏天","夏日炎炎"),
    AUTUMN("球天","秋高气爽"),
    WINTER("冬天","冰天雪地");

    //定义枚举对象的属性
    private final String seasonName;
    private final String seasonDesc;

    //定义枚举的构造方法
    private Season1(String seasonName, String seasonDesc) {
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }
}

enum Gender{
    MAN,
    WOMEN;
}