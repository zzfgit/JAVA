package DateAndTime;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/*
* JDK8之后的日期时间API
*
* */
public class JDK8DateTest {
    public static void main(String[] args) {

    }

    /*
    * localDate,localTime,localDateTime
    * localDateTime用的比较多
    * */
    @Test
    public void test1(){
        //一:实例化
        //1:now();
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(localDate);
        System.out.println(localTime);
        System.out.println(localDateTime);

        //2:of();
        //设置指定的年月日,时分秒,没有偏移量
        LocalDateTime localDateTime1 = LocalDateTime.of(2020, 10, 5, 23, 34,56);
        System.out.println(localDateTime1);

        //get();获取相关属性
        //获取当月第几天
        System.out.println(localDateTime.getDayOfMonth());

        //获取这周第几天
        System.out.println(localDateTime.getDayOfWeek());

        //获取是第几月
        System.out.println(localDateTime.getMonth());

        //withxxx();设置相关属性
        //生成一个新的时间对象,原来的时间对象不变
        //体现了Time类对象的不可变性
        LocalDate localDate1 = localDate.withMonth(5);
        System.out.println(localDate);
        System.out.println(localDate1);

        //add
        //在当前时间基础上加上指定的时间值,返回一个新的对象,源对象不变
        LocalDateTime localDateTime2 = localDateTime.plusMonths(3);
        System.out.println(localDateTime2);

        //在指定的时间基础上减去指定的时间值,返回一个新的时间对象
        LocalDateTime localDateTime3 = localDateTime.minusDays(30);
        System.out.println(localDateTime3);
    }

    //类似于Java.util.date类
    //Instant的使用
    @Test
    public void test2(){

        //获取格林威治时区的当前时间戳
        Instant now = Instant.now();
        System.out.println(now);

        //在时间戳基础上加上8个小时(获取北京时间)
        OffsetDateTime offsetDateTime = now.atOffset(ZoneOffset.ofHours(8));
        System.out.println(offsetDateTime);

        //获取自动1970年1月1日开始的毫秒
        long epochMilli = now.toEpochMilli();
        System.out.println(epochMilli);

        //根据时间戳毫秒,获取Instant实例
        Instant instant = Instant.ofEpochMilli(epochMilli);
        System.out.println(instant);
    }

    //DateTimeFormatter:格式化或解析日期,时间
    @Test
    public void test3(){
        //实例化
        //1:预定义的标准格式
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
            //格式化:将时间对象转换为字符串
        LocalDateTime localDateTime = LocalDateTime.now();
        String str1 = formatter.format(localDateTime);
        System.out.println(localDateTime);//2020-03-01T14:16:01.900900
        System.out.println(str1);//2020-03-01T14:16:01.9009

            //解析:字符串->日期
        TemporalAccessor parse = formatter.parse("2020-03-01T14:16:01.9009");
        System.out.println(parse);//{},ISO resolved to 2020-03-01T14:16:01.900900

        //2:本地化相关的格式
        DateTimeFormatter formatter1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

            //格式化
        String str2 = formatter1.format(localDateTime);
        System.out.println(str2);//2020/3/1 下午2:21

        //3:自定义的格式化格式
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("YYYY-MM-dd hh-mm-ss");

            //格式化
        String localDateTime2 = formatter2.format(localDateTime.now());
        System.out.println(localDateTime2);//2020-03-01 02-25-45

            //解析:生成一个时间对象
        TemporalAccessor localDateTime3 = formatter2.parse("2020-03-01 02-25-45");
        System.out.println(localDateTime3);
    }
}
