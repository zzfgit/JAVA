package DateAndTime;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

/*
* 日历类
* */
public class CalendarTest {
    public static void main(String[] args) {

    }
    @Test
    public void testCalendar(){
        //实例化
        //1:创建其子类对象GregorianCalendar类
        //2:调用静态方法getInstance()
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.getClass());

        //常用方法
        //get()
        int days = calendar.get(Calendar.DAY_OF_MONTH);//今天是这个月的第几天
        System.out.println(days);

        int dayOfWeak = calendar.get(Calendar.DAY_OF_WEEK);//今天是这周的第几天
        System.out.println(dayOfWeak);

        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);//今天是今年的第几天
        System.out.println(dayOfYear);

        //set()
        calendar.set(Calendar.DAY_OF_MONTH,5);//修改calendar对象本身
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));

        //add()
        calendar.add(Calendar.DAY_OF_MONTH,3);//修改calendar对象本身
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));

        //getTime() 由日历得到一个date
        Date calendayDate = calendar.getTime();
        System.out.println(calendayDate);

        //setTime() 由date转换为一个日历类对象
        Date nowDate = new Date();
        calendar.setTime(nowDate);
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));

    }

}
