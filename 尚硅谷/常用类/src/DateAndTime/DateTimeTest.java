package DateAndTime;

import org.junit.Test;

import java.util.Date;

public class DateTimeTest {
    public static void main(String[] args) {

    }
    @Test
    public void test1(){
        //返回当前时间与1970年1月1日0时0分0秒之间的毫秒差(时间戳)
        long time = System.currentTimeMillis();
        System.out.println(time);
    }

    /*
    * Date类
    * 1:两个构造器的使用
    *
    * 2:两个方法的使用
    * */
    @Test
    public void test2(){
        //构造器一:Date();
        //获取当前时间
        Date date1 = new Date();

        //年月日时分秒
        System.out.println(date1.toString());

        //时间戳
        System.out.println(date1.getTime());

        //构造器二:获取指定时间的对象
        Date date2 = new Date(120,2,3);
        System.out.println(date2);

        //创建一个sql.Date对象
        java.sql.Date date3 = new java.sql.Date(234234523452345L);
        System.out.println(date3);

        //将util.date转换成sql.date
        Date date4 = new Date();
        java.sql.Date date5 = new java.sql.Date(date4.getTime());
        System.out.println(date5);
    }

}
