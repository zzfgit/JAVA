package DateAndTime;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormateTest {
    public static void main(String[] args) {

    }
    /*
    * simpleDateFormat:对日期Date类的格式化和解析
    * 1:格式化:日期->字符串
    * 2:解析:字符串->日期
    *
    * simpleDateFormat的实例化
    * */

    @Test
    public void testSimpleDateFormat() throws ParseException {
        //实例化默认的构造器
        SimpleDateFormat sdf = new SimpleDateFormat();

        //格式化:日期->字符串
        Date date = new Date();
        String dateStr = sdf.format(date);
        System.out.println(dateStr);

        //解析:字符串 --> 日期
        String dateStr1 = "2020/3/1 上午1:01";//默认格式
        Date date1 = sdf.parse(dateStr1);
        System.out.println(date1);

        //实例化自定义格式的格式化对象:调用带参数的构造器方法
        SimpleDateFormat sdf1 = new SimpleDateFormat("YYYY/MM/DD");
        String dateStr2 = "2020/2/29";
        //解析:要求字符串的格斯必须符合日期格式化对象的格式
        Date date2 = sdf1.parse(dateStr2);
        System.out.println(date2);


    }
}
