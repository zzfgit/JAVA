package String;

import org.junit.Test;

public class StringExer2 {
    public static void main(String[] args) {
        /*
        * 获取一个字符串,在另一个字符串总出现的次数
        *
        * */
    }

    /*
    * 获取subStr在mainStr中出现的次数
    * */
    public int getCount(String mainStr,String subStr){
        int mainLength = mainStr.length();
        int subLength = subStr.length();

        int count = 0;
        int index = 0;
        if (mainLength >= subLength) {
            //方式一:
//            while ((index = mainStr.indexOf(subStr)) != -1) {
//                count ++;
//                mainStr = mainStr.substring(index + subStr.length());
//            }

//            方式二:
            while ((index = mainStr.indexOf(subStr,index)) != -1){
                count ++;
                index += subLength;
            }

            return count;
        }else{
            return 0;
        }
    }

    @Test
    public void testGetCount(){
        String mainStr = "assdfasdfghdfghasdfghdfgas";
        String substr = "as";
        int count = getCount(mainStr,substr);
        System.out.println(count);
    }
}
