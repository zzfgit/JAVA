package String;

public class StringBufferTest {

    public static void main(String[] args) {
        StringBuffer s1 = new StringBuffer();
        System.out.println(s1.length());


//        StringBuffer和StringBuilder功能一样,只不过buffer是线程安全的

        test();

    }

    static public void test(){
        StringBuffer s1 = new StringBuffer("abc");
        s1.append("defhijkol");
        System.out.println(s1);

        s1.delete(2,4);
        System.out.println(s1);

        s1.replace(2,3,"hello");
        System.out.println(s1);

        s1.insert(2,"aaa");
        System.out.println(s1);

        StringBuffer s2 = new StringBuffer("abc");
        s2.reverse();
        System.out.println(s2);

        StringBuffer s3 = new StringBuffer("abc");
        String s4 = s3.substring(1);
        System.out.println(s4);
    }
}
