package String;

import org.junit.Test;

public class StringExer3 {
    public static void main(String[] args) {
    }

    /*
     * 获取两个字符串中的最大子串
     *
     * */
    public String getMaxSameString(String str1, String str2) {

        String maxStr = str1.length() >= str2.length() ? str1 : str2;
        String minStr = str1.length() < str2.length() ? str1 : str2;

        int length = minStr.length();
        for (int i = 0; i < length; i++) {
            for (int x = 0,y = length -i;y <= length;x ++,y ++){
                String subStr = minStr.substring(x,y);
                if (maxStr.contains(subStr)) {
                    return subStr;
                }
            }
        }
        return null;
    }

    @Test
    public void testgetMaxSameString(){
        String str1 = "assasdfasdjhkjsdfjhsdfgkjhlskdfhg";
        String str2 = "adfasdfasdfjhasdfh";
        String maxSamaString = getMaxSameString(str1,str2);
        System.out.println(maxSamaString);
    }
}
