package String;

import org.junit.Test;

/*
* String的使用
*
* */
public class StringTest {
/*
* String:字符串,使用一对""来标识
* 1:String声明为final,不可被集成
* 2:String实现了Serialzable接口:标识字符串是支持序列化的
* 3:String实现了Comparable接口,可以比较大小
* 4:String内部定义了finel char[] value数组,用于存储字符串数据
* 5:String:代表一个不可变的字符序列,简称:不可变性
*   1)当对字符串重新赋值时,需要重新指定内存区域赋值
*   2)
*   3)当调用字符串的replace()方法时,也会生成一个新的字符串
* 6:通过字面量给一个字符串赋值,此时字符串的值声明在字符串常量池中
* 7:字符串常量池中,不会存储相同内容的字符串
*
* */
    @Test
    public void test1(){
        String s1 = "abc";//字面量定义方式
        String s2 = "abc";
        System.out.println("s1 是否等于 s2:" + (s1 == s2));
        s1 = "hello";
        System.out.println("s1 是否等于 s2:" + (s1 == s2));

        System.out.println(s1);
        System.out.println(s2);

        String s4 = s2.replace("a", "m");
        System.out.println(s2);
        System.out.println(s4);
    }

    @Test
    public void test3(){
        String s1 = "javaEE";
        String s2 = "hadoop";

        String s3 = "javaEEhadoop";
        String s4 = "javaEE" + "hadoop";
        String s5 = s1 + "hadoop";
        String s6 = "javaEE" + s2;
        String s7 = s1 + s2;

        System.out.println(s3 == s4);//true/*/*/*/*/**/*/*/*/*/
        System.out.println(s3 == s5);//false
        System.out.println(s3 == s6);//false
        System.out.println(s5 == s6);//false
        System.out.println(s5 == s7);//false

        String s8 = s5.intern();
        System.out.println(s3 == s8);//返回值s8使用常量池中已经存在的字符串

        /*
        * 常量与常量的拼接结果在常量池,并且常量池中不会存在两个相同的常量
        * 只要拼接的值中有一个是变量,结果就在堆中
        * 如果拼接的结果是调用intern()方法得到的,返回值就就是常量池中的值
        * */
    }
}
