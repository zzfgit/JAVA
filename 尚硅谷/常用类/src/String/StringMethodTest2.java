package String;

public class StringMethodTest2 {
    public static void main(String[] args) {

        //判断是否以指定字符结尾
        String s1 = "hello world";
        boolean b1 = s1.endsWith("d");
        System.out.println(b1);


        //判断是否以指定字符开头
        boolean b2 = s1.startsWith("hell");
        System.out.println(b2);

        //字符串替换
        String s2 = "hello world";
        String s3 = s2.replace("o","888");
        System.out.println(s3);

        //切片
        String s4 = "asdk,aslkdjf;lk,askdjflk,adfjlk,aksdjflk";
        String[] strarr = s4.split(",");
        for (String str :
                strarr) {
            System.out.println(str);
        }
    }
}
