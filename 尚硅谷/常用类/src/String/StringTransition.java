package String;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/*
* String类与其他结构之间的转换
*
* */
public class StringTransition {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //字符串转换成int类型
        String s1 = "123";
        int i1 = Integer.parseInt(s1);
        System.out.println(i1);//123

        //基本数据类型转换成String类型
        String s2 = String.valueOf(i1);
        System.out.println(s2);//123

        //String与char[]之间的转换
        String s3 = "abc123";
        char[] s3Arr = s3.toCharArray();
        for (char c :
                s3Arr) {
            System.out.println(c);
        }

        //char[]转换成String
        char[] charArr1 = new char[]{'1','3','5'};
        String s4 = new String(charArr1);
        System.out.println(s4);

        //String 与byte[]之间的转换
        String s5 = "abc123中国";
        byte[] byteArr1 = s5.getBytes();//使用默认的字符集
        System.out.println(Arrays.toString(byteArr1));

        byte[] byteArr2 = s5.getBytes("gbk");//使用指定的字符集
        System.out.println(Arrays.toString(byteArr2));
        String s6 = new String(byteArr2,"gbk");
        System.out.println(s6);
    }
}
