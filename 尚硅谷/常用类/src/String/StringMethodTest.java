package String;

public class StringMethodTest {

    public static void main(String[] args) {
        String s1 = "Hello World";
        System.out.println(s1.length());//11
        System.out.println(s1.charAt(0));//h
        System.out.println(s1.isEmpty());//false
        System.out.println(s1.toLowerCase());//hello world
        System.out.println(s1.toUpperCase());//HELLO WORLD
        System.out.println(s1);//Hello World  s1并没有改变

        String s2 = "     Hello World";
        System.out.println(s2.trim());//Hello World 去除首位空格


        String s3 = "abc";

        String s4 = s3.concat("def");
        System.out.println(s4);///abcdef®

        String s5 = "abc";
        String s6 = "abd";
        System.out.println(s5.compareTo(s6));//-1

        String s7 = "Hello World";
        String s8 = s7.substring(2);
        System.out.println(s8);//llo World

        String s9 = s7.substring(3,4);
        System.out.println(s9);//l



    }
}
