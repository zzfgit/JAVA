package whatIsGenericity;

import java.util.ArrayList;

/*
* 泛型
* 泛型就是编写模板代码来适应任意类型；

泛型的好处是使用时不必对类型进行强制转换，它通过编译器对类型进行检查；

注意泛型的继承关系：可以把ArrayList<Integer>向上转型为List<Integer>（T不能变！），但不能把ArrayList<Integer>向上转型为ArrayList<Number>（T不能变成父类）。*/
public class Genericity {
    public static void main(String[] args) {

        ArrayList<String> strArr = new ArrayList<String>();
        strArr.add("a");
        strArr.add("b");
        strArr.add("c");

        String str1 = strArr.get(0);
        System.out.println(strArr);
        System.out.println(str1);

        ArrayList<Integer> intArr = new ArrayList<Integer>();
        intArr.add(1);
        intArr.add(2);
        intArr.add(3);

        Integer int1 = intArr.get(0);
        System.out.println(intArr);
        System.out.println(int1);
    }
}
